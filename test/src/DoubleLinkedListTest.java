package src;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;

import model.data_structures.DoubleLinkedList;
import model.data_structures.Nodo;
import model.logic.Ruta;


public class DoubleLinkedListTest {

	
	private DoubleLinkedList<Ruta> listaRutas;
	private Ruta ruta1;
	private Ruta ruta2;
	private Ruta ruta3;
	private Ruta ruta4;
	
	@Test
	public void test() {
		testAgregarLista();
		testDelete();
		testIterator();
		testIntercambiar();
	}

	public void setupEscenario1()
	{
		
		listaRutas = new DoubleLinkedList<>();
		
		ruta1 = new Ruta(1, "Agencia", "k23", "PortalEldorado", "", 1, "url", "", "");
		ruta2 = new Ruta(2, "Agencia", "j24", "Universidades", "", 1, "url2", "", "");
		ruta3 = new Ruta(3, "Agencia", "b74", "PortalNorte", "", 1, "url3", "", "");
		ruta4 = new Ruta(4, "Agencia", "d70", "Portal80", "", 1, "url4", "", "");
		
		
		listaRutas.add(ruta1);
		listaRutas.add(ruta2);
		listaRutas.add(ruta3);
		listaRutas.addAtEnd(ruta4);
		
	}
	
	public void setupEscenario2()
	{
		
		listaRutas = new DoubleLinkedList<>();
		
		ruta1 = new Ruta(1, "Transmilenio", "k23", "PortalEldorado", "", 1, "url", "", "");
		ruta2 = new Ruta(2, "Transmilenio", "j24", "Universidades", "", 1, "url2", "", "");
		ruta3 = new Ruta(3, "Transmilenio", "b74", "PortalNorte", "", 1, "url3", "", "");
		ruta4 = new Ruta(4, "Transmilenio", "d70", "Portal80", "", 1, "url4", "", "");
		

		
		listaRutas.addAtEnd(ruta1);
		listaRutas.addAtEnd(ruta2);
		listaRutas.addAtEnd(ruta3);
		listaRutas.addAtEnd(ruta4);
		
	}
	
	
	public void setupEscenario3()
	{
		
		listaRutas = new DoubleLinkedList<>();
		
		ruta1 = new Ruta(1, "Transmilenio", "k23", "PortalEldorado", "", 1, "url", "", "");
		ruta2 = new Ruta(2, "Transmilenio", "j24", "Universidades", "", 1, "url2", "", "");
		ruta3 = new Ruta(3, "Transmilenio", "b74", "PortalNorte", "", 1, "url3", "", "");
		ruta4 = new Ruta(4, "Transmilenio", "d70", "Portal80", "", 1, "url4", "", "");
		
		
		
		listaRutas.addAtk(ruta1, 0);
		listaRutas.addAtk(ruta2, 1);
		listaRutas.addAtk(ruta3, 1);
		listaRutas.addAtk(ruta4, 2);
		
	}
	
	
	public void testAgregarLista()
	{
		
		setupEscenario1();
		
	
	
		assertTrue("El numero no es el esperado" , listaRutas.getSize()== 4);
		assertTrue("El elemento no es el esperado", listaRutas.getElement(0).equals(ruta3));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(2).equals(ruta1));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(3).equals(ruta4));

		
		setupEscenario2();
		
		
		assertTrue("El numero no es el esperado" , listaRutas.getSize()== 4);
		assertTrue("El elemento no es el esperado", listaRutas.getElement(0).equals(ruta1));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(1).equals(ruta2));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(2).equals(ruta3));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(3).equals(ruta4));
		
		
		setupEscenario3();

		assertTrue("El numero no es el esperado" , listaRutas.getSize()== 4);
		assertTrue("El elemento no es el esperado", listaRutas.getElement(0).equals(ruta1));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(1).equals(ruta3));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(3).equals(ruta2));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(2).equals(ruta4));
	}
	
	
	
	public void testDelete()
	{
		
		setupEscenario1();
		
		
		listaRutas.delete(); 
		
		assertTrue("El numero no es el esperado", listaRutas.getSize() == 3);
		assertTrue("El primer elemento no es el esperado", listaRutas.getElement(0).equals(ruta2));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(1).equals(ruta1));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(2).equals(ruta4));
		
		listaRutas.deleteAtk(2);
		
		assertTrue("El numero no es el esperado", listaRutas.getSize() == 2);
		assertTrue("El primer elemento no es el esperado", listaRutas.getElement(0).equals(ruta2));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(1).equals(ruta1));

		listaRutas.delete();
		assertTrue("El numero no es el esperado", listaRutas.getSize() == 1);
		assertTrue("El elemento no es el esperado", listaRutas.getElement(0).equals(ruta1));
		
		setupEscenario1();
		
		listaRutas.deleteAtk(0);
		assertTrue("El elemento no es el esperado", listaRutas.getElement(0).equals(ruta2));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(1).equals(ruta1));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(2).equals(ruta4));
		
		
		listaRutas.deleteAtk(1);
		assertTrue("El numero no es el esperado", listaRutas.getSize() == 2);
		assertTrue("El elemento no es el esperado", listaRutas.getElement(0).equals(ruta2));
		assertTrue("El elemento no es el esperado", listaRutas.getElement(1).equals(ruta4));
		
	}
	
	
	
	public void testIntercambiar()
	{
		setupEscenario2();
		Nodo<Ruta> nodo1 = listaRutas.getNodo(0);
		
		assertTrue(nodo1.darElemento()== ruta1);
		
		Nodo<Ruta> nodo2 = listaRutas.getNodo(1);
		
		listaRutas.intercambiar(nodo1, nodo2);
		
		nodo1 = listaRutas.getNodo(0);
		nodo2 = listaRutas.getNodo(1);
		
		assertTrue(nodo1.darElemento() == ruta2);
		assertTrue(nodo2.darElemento() == ruta1);
		
		
		
		Nodo<Ruta> nodo3 = listaRutas.cola();
		
		listaRutas.intercambiar(nodo1, nodo3);
		
		assertTrue(listaRutas.cabeza().darElemento() == ruta4);
		assertTrue(listaRutas.cola().darElemento() == ruta2);
		
		nodo1 = listaRutas.getNodo(0);
		nodo2 = listaRutas.getNodo(3);
		
		listaRutas.intercambiar(nodo2, nodo1);
		
		assertTrue(listaRutas.cabeza().darElemento() == ruta2);
		assertTrue(listaRutas.getNodo(3).darElemento() == ruta4);
		
		Ruta ruta5 = new Ruta(4, "Transmilenio", "d70", "Portal80", "", 1, "url4", "", "");
		listaRutas.addAtEnd(ruta5);
		
		nodo1 = listaRutas.getNodo(1);
		nodo2 = listaRutas.getNodo(3);
		
		listaRutas.intercambiar(nodo1, nodo2);
		
		assertTrue(listaRutas.getElement(1) == ruta4);
		assertTrue(listaRutas.getElement(3) == ruta1);
		
		setupEscenario3();

	}
	
	public void testIterator()
	{
		
		setupEscenario2();
		
		Iterator<Ruta> iter = listaRutas.iterator(); 
		
		assertTrue("El elemento no es el esperado", iter.next().equals(ruta1));
		assertTrue("El elemento no es el esperado", iter.next().equals(ruta2));
		assertTrue("El elemento no es el esperado", iter.next().equals(ruta3));
		assertTrue("El elemento no es el esperado", iter.next().equals(ruta4));
		//assertTrue("El elemento no es el esperado", iter.next()==null);
		assertFalse(iter.hasNext());
		
	}
	
	
	

}
