package src;

import static org.junit.Assert.*;

import org.junit.Test;


import model.data_structures.LinkedStack;
import model.logic.Ruta;


public class LinkedStackTest {

	private LinkedStack<Ruta> listaRutas;
	private Ruta ruta1;
	private Ruta ruta2;
	private Ruta ruta3;
	private Ruta ruta4;


	@Test
	public void test() {


		testPush();
		testPop();

	}


	public void setupEscenario1()
	{

		listaRutas = new LinkedStack<>();

		ruta1 = new Ruta(1, "Agencia", "k23", "PortalEldorado", "", 1, "url", "", "");
		ruta2 = new Ruta(2, "Agencia", "j24", "Universidades", "", 1, "url2", "", "");
		ruta3 = new Ruta(3, "Agencia", "b74", "PortalNorte", "", 1, "url3", "", "");
		ruta4 = new Ruta(4, "Agencia", "d70", "Portal80", "", 1, "url4", "", "");

		listaRutas.push(ruta1);
		listaRutas.push(ruta2);
		listaRutas.push(ruta3);
		listaRutas.push(ruta4);


	}


	public void testPush()
	{
		setupEscenario1();
		assertTrue("El numero de elementos no es el esperado" , listaRutas.size() == 4);


	}


	public void testPop()
	{
		setupEscenario1();
		
		assertTrue("El elemento no es el esperado", listaRutas.pop() == ruta4);
		assertTrue("El elemento no es el esperado", listaRutas.pop() == ruta3);
		assertTrue("El elemento no es el esperado", listaRutas.pop() == ruta2);
		assertTrue("El elemento no es el esperado", listaRutas.pop() == ruta1);
		
		assertTrue("El numero de elementos no es el esperado", listaRutas.size() == 0);
	}
	

}
