package src;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.DoubleLinkedList;
import model.data_structures.Nodo;
import model.logic.OrdenadorListas;

public class OrdenadorListasTest {

	
	Integer uno;
	Integer dos;
	Integer tres;
	Integer cuatro;
	Integer cinco; 
	Integer seis; 
	Integer siete;
	Integer ocho;
	Integer nueve;
	Integer diez;

	OrdenadorListas ordenador;
	
	DoubleLinkedList<Integer> lista; 
	DoubleLinkedList<Integer> listaOrdenada;
	@Test
	public void test() {
		
		testMerge();
		testInsertion();
		
	}

	
	public void setupEscenario1()
	{
		lista = new DoubleLinkedList<>();
		listaOrdenada = new DoubleLinkedList<>(); 
		ordenador = new OrdenadorListas();
		
		uno = new Integer(1);
		dos = new Integer(2);
		tres = new Integer(3);
		cuatro= new Integer(4);
		cinco = new Integer(5);
		seis= new Integer(6);
		siete = new Integer(7);
		ocho = new Integer(8);
		nueve = new Integer(9);
		diez = new Integer(10);
		
		
		lista.addAtEnd(tres);
		lista.addAtEnd(uno);
		lista.addAtEnd(siete);
		lista.addAtEnd(seis);
		lista.addAtEnd(dos);
		lista.addAtEnd(diez);
		lista.addAtEnd(cinco);
		lista.addAtEnd(nueve);
		lista.addAtEnd(ocho);
		lista.addAtEnd(cuatro);
		
	}
	
	
	public void testMerge()
	{
		
		setupEscenario1();
		

		
		listaOrdenada = ordenador.ordenarPorMerge(lista);
		
		Nodo<Integer> cabeza = ordenador.cabezaMerge(lista); 
	
		
		int i = 1;
		for (Integer integer : listaOrdenada) {
			
			
			//System.out.println("Valor esperado: " + i + " 		Valor en la lista: " + integer.intValue());			
			assertTrue(integer.intValue() == i);
		
			
			
			i++;
		}
		
		
		
	}
	
	public void testInsertion()
	{
		
		setupEscenario1();
		
		 ordenador.ordernarInsertion(lista);
		
		
		
		int i = 1;
		for (Integer integer : lista) {
			
			//System.out.println("Valor esperado: " + i + " 		Valor en la lista: " + integer.intValue());
			
			
			assertTrue(integer.intValue() == i);
			
			i++;
		}
		
	}
	
}
