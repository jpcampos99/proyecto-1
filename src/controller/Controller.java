package controller;


import model.data_structures.DoubleLinkedList;
import model.logic.Ruta;
import model.logic.STSManager;
import model.logic.Trip;
import model.vo.RetardoVO;
import model.vo.StopVO;
import model.vo.TransferVO;
import api.ISTSManager;

public class Controller {
	
	/**
	 * Reference to the routes and stops manager
	 */
	private static ISTSManager  manager = new STSManager();

	public static void ITScargarGTFS() {
		manager.ITScargarGTFS();	
	}

	
	public static void ITScargarTR(String pfecha)
	{		
		manager.ITScargarTR(pfecha);
		
	}
	
	public static DoubleLinkedList<Ruta> ITSrutasPorEmpresa(String nombreEmpresa, String fecha)
	{
		return manager.ITSrutasPorEmpresa(nombreEmpresa, fecha);
	}

	public static DoubleLinkedList<Trip> ITSviajesRetrasadosRuta(String idRuta, String fecha)
	{
		
		
		return manager.ITSviajesRetrasadosRuta(idRuta, fecha);
	}


	public static DoubleLinkedList<StopVO> ITSparadasRetrasadasFecha(String fechaCase5) {


		return manager.ITSparadasRetrasadasFecha(fechaCase5);
	}


	public static DoubleLinkedList<TransferVO> ITStransbordosRuta(String idRutaCase6, String fechaCase6) {

		return manager.ITStransbordosRuta(idRutaCase6);
	}


	public static DoubleLinkedList<DoubleLinkedList<Trip>> ITSrutasPlanUtilizacion(DoubleLinkedList<String> secuenciaDeParadas, String fechaCase7,
			String horaInicioCase7, String horaFinCase7) 
	{
		
		return manager.ITSrutasPlanUtilizacion(fechaCase7, horaInicioCase7, horaFinCase7, secuenciaDeParadas);
	}


	public static DoubleLinkedList<RetardoVO> ITSretardosViaje(String fechaCase14, String idViajeCase14) {
		
		return manager.ITSretardosViaje(fechaCase14, idViajeCase14);
	}


	public static DoubleLinkedList<StopVO> ITSparadasCompartidas(String fechaCase15) {
		// TODO Auto-generated method stub
		return manager.ITSparadasCompartidas(fechaCase15);
	}


	public static DoubleLinkedList<Ruta> ITSrutasPorEmpresaParadas(String empresaCase8, String fechaCase8) {
		// TODO Auto-generated method stub
		return manager.ITSrutasPorEmpresaParadas(empresaCase8, fechaCase8);
	}


	public static DoubleLinkedList<Trip> ITSviajesRetrasoTotalRuta(String idRutaCase9, String fechaCase9) {
		// TODO Auto-generated method stub
		return manager.ITSviajesRetrasoTotalRuta(idRutaCase9, fechaCase9);
	}


	public static DoubleLinkedList<String> ITSretardoHoraRuta(String idRutaCase10, String fechaCase10) {
		// TODO Auto-generated method stub
		return manager.ITSretardoHoraRuta(idRutaCase10, fechaCase10);
	}


	public static DoubleLinkedList<Trip> ITSbuscarViajesParadas(String idOrigen11, String idDestino11,
			String fechaCase11, String horaInicio11, String horaFin11) {
		// TODO Auto-generated method stub
		return manager.ITSbuscarViajesParadas(idOrigen11, idDestino11, fechaCase11, horaInicio11, horaFin11);
	}
	
	
	
}
