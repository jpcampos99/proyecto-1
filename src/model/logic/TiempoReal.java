package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.data_structures.DoubleLinkedList;
import model.data_structures.Nodo;
import model.vo.BusUpdateVO;
import model.vo.Schedule;
import model.vo.StopEstimateVO;
import model.vo.StopVO;



public class TiempoReal <T extends Comparable<T>>{


	private DoubleLinkedList<BusUpdateVO> listaBusUpdates;
	private DoubleLinkedList<StopEstimateVO> listaStopEstimates;

	private OrdenadorListas<T> ordenador;

	
	

	public TiempoReal()
	{
		listaBusUpdates = new DoubleLinkedList<>(); 
		listaStopEstimates = new DoubleLinkedList<>();
		ordenador = new OrdenadorListas<T>();
		
		
	
	}


	public void cargarBusUpdates(File file)
	{

		Gson gson = new GsonBuilder().create();
		BufferedReader reader = null; 



		try
		{

			reader = new BufferedReader(new FileReader(file));


			BusUpdateVO[] update = gson.fromJson(reader, BusUpdateVO[].class);

			for (int i = 0; i < update.length; i++) {

				listaBusUpdates.add(update[i]);


			}


		}catch(Exception e)
		{
			e.printStackTrace();

		}finally {
			try {
				if(reader != null)
				{
					reader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}




	public void cargarStopEstimates(File file)
	{
		Gson gson =  new GsonBuilder().create();
		BufferedReader reader = null; 



		try
		{

			reader = new BufferedReader(new FileReader(file));

			String linea = reader.readLine();


			if(linea.startsWith("{\"Code\""))
			{

				return;
			}

			reader = new BufferedReader(new FileReader(file));

			//			System.out.println("Esta en la file" + file.getName());
			StopEstimateVO[] update = gson.fromJson(reader, StopEstimateVO[].class);

			for (int i = 0; i < update.length; i++) {

				String nombreArchivo = file.getName(); 
				int stopCode = Integer.parseInt(nombreArchivo.substring(20,25 ));
				update[i].setStopCode(stopCode);
				listaStopEstimates.addAtEnd(update[i]);


			}


		}catch(Exception e)
		{
			e.printStackTrace();

		}finally {
			try {
				if(reader != null)
				{
					reader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}



	}

	//no llega a mucho
	public DoubleLinkedList<String>viajesRetrasadosRuta(String routeName)
	{

		DoubleLinkedList<String> rutas = new DoubleLinkedList<>(); 

		for (StopEstimateVO stopEstimate : listaStopEstimates) 
		{
			if(stopEstimate.getRouteName().equals(routeName))
			{
				List<Schedule> schedules = stopEstimate.getSchedules(); 

				for (Schedule schedule : schedules) 
				{
					if(schedule.getScheduleStatus().equals("-"))
					{



					}
				}
			}

		}

		return rutas;
	}

	//Hace join sin repeticiones que est�n seguidas
	public DoubleLinkedList<Trip> joinBusUpdatesATrips(DoubleLinkedList<Trip> pListaTrips)
	{
	
		
		Nodo<Trip> nodoTrip = pListaTrips.cabeza();
		
		while (nodoTrip != null) {
	
			Trip trip = nodoTrip.darElemento();
			int contador = 0;
			
			for (BusUpdateVO busUpdate : listaBusUpdates) 
			{
				if(trip.getTripId() == busUpdate.darTripId())
				{

					BusUpdateVO ultimoAgregado = trip.darBusUdpates().primerElemento();

					if(ultimoAgregado == null)
					{
						trip.addABusUpdates(busUpdate);
					}
					else if(!ultimoAgregado.darRecordedTime().equals(busUpdate.darRecordedTime()))
					{

						trip.addABusUpdates(busUpdate);
					}
				}

				contador++;

			}
			if(contador == listaBusUpdates.getSize())
			{	
				
				if(trip.darBusUdpates().getSize() == 0){
					Nodo<Trip> nodoAEliminar = nodoTrip;
					nodoTrip = nodoTrip.darSiguiente();
					pListaTrips.eliminarNodo(nodoAEliminar);
				}else{
						ordenador.ordernarInsertion((DoubleLinkedList<T>) trip.darBusUdpates());
						trip.crearRetrasos();
						nodoTrip = nodoTrip.darSiguiente();
				}
				
				
			}
			else {
				nodoTrip = nodoTrip.darSiguiente();
			}

		}



		return pListaTrips;
	}

	
	public DoubleLinkedList<StopVO> joinStopsStopEstimates(DoubleLinkedList<StopVO> pStops)
	{
		
		Nodo<StopVO> nodoStop = pStops.cabeza();
		Nodo<StopEstimateVO> nodoE = listaStopEstimates.cabeza(); 
		
		DoubleLinkedList<StopVO> stopConRetraso = new DoubleLinkedList<>();
		
		while(nodoStop != null && nodoE != null)
		{

			StopVO stop = nodoStop.darElemento(); 
			StopEstimateVO estimate = nodoE.darElemento();
//			System.out.println("Stop " + stop.getStopCode() );
//			System.out.println("Estimate " + estimate.getStopCode());
			
			if(stop.getStopCode() == estimate.getStopCode())
			{
				
				List<Schedule> schedules = estimate.getSchedules();

				for (Schedule schedule : schedules) {
					
					if(schedule.getScheduleStatus().equals("-"))
					{
						stop.aumentarIncidentes(); 
					}
				}
				
				
				nodoE = nodoE.darSiguiente(); 
			}else{
				
				//Avanzar en stops
				//System.out.println("Avanzar en stops");
				if(stop.getNumeroIncidentes() != 0) // Si no tiene incidentes lo elimina
				{
					//System.out.println("Elimina nodo");
				
					stopConRetraso.add(stop);
					
					nodoStop = nodoStop.darSiguiente(); 
				
				
				}else{
					nodoStop = nodoStop.darSiguiente(); 
				}
				
			}
			
			
			
			
		}
		
		
		
		return stopConRetraso; 
		
		
		
		
		
	}

	public DoubleLinkedList<BusUpdateVO> darListaBusUpdates()
	{
		return listaBusUpdates;

	}


	public DoubleLinkedList<StopEstimateVO> darListaStopEstimates()
	{
		return listaStopEstimates; 
	}


	public DoubleLinkedList<Integer> darParadasCompartidas() {

		DoubleLinkedList<Integer> listaStopCodes = new DoubleLinkedList<>();
		
		Nodo<StopEstimateVO> nodoE = listaStopEstimates.cabeza(); 
		
		
		while(nodoE != null){
			
			
			Nodo<StopEstimateVO> signodo = null; 
			if(nodoE.darSiguiente()!= null)
			{
				signodo = nodoE.darSiguiente(); 
			}
			
			StopEstimateVO sig = signodo.darElemento();
			StopEstimateVO actual = nodoE.darElemento();
			
			if(actual.getStopCode() == sig.getStopCode())
			{

				if(!actual.getRouteNo().equals(sig.getRouteNo()))
				{
					listaStopCodes.add(actual.getStopCode());
				}
				
			}
			
			
			nodoE = nodoE.darSiguiente();
			
		}
		
		
		
		return listaStopCodes;
	}

}
