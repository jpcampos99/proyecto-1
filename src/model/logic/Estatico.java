package model.logic;

import java.io.BufferedReader;
import java.io.DataOutput;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ServiceConfigurationError;

import model.data_structures.DoubleLinkedList;

import model.data_structures.Nodo;
import model.vo.CalendarDateVO;
import model.vo.CalendarVO;
import model.vo.RetardoVO;
import model.vo.StopTimesVO;
import model.vo.StopVO;
import model.vo.TransferVO;




public class Estatico {



	private DoubleLinkedList<CalendarVO> listaCalendario;
	private DoubleLinkedList<Ruta> listaRutas;
	private DoubleLinkedList<StopVO> listaStops;
	private DoubleLinkedList<StopTimesVO> listaStopTimes;
	private DoubleLinkedList<Trip> listaTrips;
	private DoubleLinkedList<TransferVO> listaTransfers;
	private DoubleLinkedList<CalendarDateVO> listaCalendarDates;

	/*
	 * 
	 * Cargar
	 * 
	 */


	public void cargarCalendario()
	{

		listaCalendario = new DoubleLinkedList<>(); 

		BufferedReader lector = null;

		try{
			lector = new BufferedReader(new FileReader("./data/static/calendar.txt"));
			String linea = lector.readLine();
			linea = lector.readLine();



			while(linea!= null)
			{
				String[] partes = linea.split(",");


				int service_id = Integer.parseInt(partes[0]);
				String monday =  partes[1];
				String tuesday = partes[2];
				String wednesday = partes[3].trim();
				String thursday = partes[4];
				String friday = partes[5]; 
				String saturday = partes[6];
				String sunday = partes[7];
				String start_date = partes[7];
				String 	end_date = partes[8];     

				CalendarVO calendario = new CalendarVO(service_id, monday, tuesday, wednesday, thursday, friday, saturday, sunday, start_date, end_date);

				listaCalendario.add(calendario);



				linea = lector.readLine();
			}

			lector.close();

		}catch (Exception e) {
			e.printStackTrace();

		}
	}


	public void cargarCalendarDate()
	{
		listaCalendarDates = new DoubleLinkedList<>(); 

		BufferedReader lector = null;

		try{
			lector = new BufferedReader(new FileReader("./data/static/calendar_dates.txt"));
			String linea = lector.readLine();
			linea = lector.readLine();



			while(linea!= null)
			{
				String[] partes = linea.split(",");


				int serviceId = Integer.parseInt(partes[0]);
				String date = partes[1];
				int exception = Integer.parseInt(partes[2]);

				CalendarDateVO calendario = new CalendarDateVO(serviceId, date, exception);

				listaCalendarDates.add(calendario);

				linea = lector.readLine();
			}



		}catch (Exception e) {
			e.printStackTrace();

		}




	}


	public void cargarRutas()
	{
		listaRutas = new DoubleLinkedList<>();

		BufferedReader lector = null;

		try{
			lector = new BufferedReader(new FileReader("./data/static/routes.txt"));
			String linea = lector.readLine();
			linea = lector.readLine();



			while(linea!= null)
			{
				String[] partes = linea.split(",");


				int route_id = Integer.parseInt(partes[0]);
				String agency_id = partes[1];
				String route_short_name = partes[2];
				String route_long_name = partes[3].trim();
				String route_desc = partes[4];
				int route_type = Integer.parseInt(partes[5]); 
				String route_url = partes[6];
				String route_color = partes[7];
				String 	route_text_color = partes[8];     


				Ruta ruta = new Ruta(route_id, agency_id, route_short_name, route_long_name, route_desc, route_type, route_url, route_color, route_text_color);

				listaRutas.add(ruta);


				linea = lector.readLine();
			}



		}catch (Exception e) {

			e.printStackTrace();
		}finally {
			try {
				lector.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}




	}


	public void cargarStops()
	{

		listaStops = new DoubleLinkedList<>();

		BufferedReader lector = null;


		try{
			lector = new BufferedReader(new FileReader("./data/static/stops.txt"));
			String linea = lector.readLine();
			linea = lector.readLine();
			while(linea!= null)
			{


				String[] partes = linea.split(",");

				int stop_id = Integer.parseInt(partes[0]);
				int stop_code = (partes[1].equals(" ") )? 0 : Integer.parseInt(partes[1]); 
				String stop_name = partes[2];
				String stop_desc = partes[3]; 
				double stop_lat = Double.parseDouble(partes[4]);
				double stop_lon = Double.parseDouble(partes[5]); 
				String zone_id = partes[6];
				String stop_url = partes[7];
				String location_type = partes [8];
				int parent_station = Integer.parseInt(partes[8]);

				StopVO stop = new StopVO(stop_id, stop_code, stop_name, stop_desc, stop_lat, stop_lon, zone_id, stop_url, location_type, parent_station);

				listaStops.add(stop);

				linea = lector.readLine();
			}

		}catch (Exception e) {

			e.printStackTrace();
		}finally {
			if(lector != null)
			{
				try {
					lector.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	//Agrega en el orden en que estan en el archivo
	public void cargarStopTimes()
	{

		listaStopTimes = new DoubleLinkedList<>();

		BufferedReader lector = null;


		try{
			lector = new BufferedReader(new FileReader("./data/static/stop_times.txt"));
			String linea = lector.readLine();
			linea = lector.readLine();
			while(linea!= null)
			{
				String[] partes = linea.split(",");

				int trip_id = Integer.parseInt(partes[0]); 
				String arrival_time = partes[1];
				String departure_time = partes[2];
				int stop_id = Integer.parseInt(partes[3]); 
				int stop_sequence = Integer.parseInt(partes[4]);
				String stop_headsign = partes[5];
				int pickup_type = Integer.parseInt(partes[6]);
				int drop_off_type = Integer.parseInt(partes[7]);

				double shape_dist_traveled = 0;

				if(partes.length > 8)
				{
					shape_dist_traveled = Double.parseDouble(partes[8]); 
				}

				StopTimesVO stopTime = new StopTimesVO(trip_id, arrival_time, departure_time, stop_id, stop_sequence, stop_headsign, pickup_type, drop_off_type, shape_dist_traveled);


				if(listaStopTimes.estaVacio())
				{
					listaStopTimes.addAtEnd(stopTime);
					listaStopTimes.addBookmark();
				}
				else if(listaStopTimes.elementoCola().getTripId() != trip_id)
				{
					listaStopTimes.addAtEnd(stopTime);
					listaStopTimes.addBookmark(); 
				}
				else{


					listaStopTimes.addAtEnd(stopTime);

				}


				linea = lector.readLine();
			}


		}catch (Exception e) {

			e.printStackTrace();
		}finally {
			if(lector != null)
			{
				try {
					lector.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}





	}


	public void cargarTrips()
	{

		listaTrips = new DoubleLinkedList<>();

		BufferedReader lector = null;


		try{
			lector = new BufferedReader(new FileReader("./data/static/trips.txt"));
			String linea = lector.readLine();
			linea = lector.readLine();


			while(linea!= null)
			{
				String[] partes = linea.split(",");


				int route_id = Integer.parseInt(partes[0]);
				int service_id= Integer.parseInt(partes[1]);
				int trip_id = Integer.parseInt(partes[2]);
				String trip_headsign = partes[3];
				String trip_short_name = partes[4];
				int direction_id = Integer.parseInt(partes[5]); 
				int block_id = Integer.parseInt(partes[6]);
				int	shape_id = Integer.parseInt(partes[7]);
				boolean wheelchair_accesible = partes[8].equals("0") ? false : true; 
				boolean bikes_allowed = partes[9].equals("0") ? false : true; 


				linea = lector.readLine();

				Trip trip = new Trip(route_id, service_id, trip_id, trip_headsign, trip_short_name, direction_id, shape_id, wheelchair_accesible, bikes_allowed);


				listaTrips.add(trip);

				for (Ruta ruta: listaRutas) {



					if(ruta.darId() == route_id)
					{
						ruta.addTrip(trip);
						trip.setRuta(ruta);

					}

				}




			}



		}catch (Exception e) {

			e.printStackTrace();
		}finally {
			if(lector != null)
			{
				try {
					lector.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}


	public void cargarTransfers()
	{

		listaTransfers = new DoubleLinkedList<>();

		BufferedReader lector = null;


		try{
			lector = new BufferedReader(new FileReader("./data/static/transfers.txt"));
			String linea = lector.readLine();
			linea = lector.readLine();
			while(linea!= null)
			{
				String[] partes = linea.split(",");

				int from_stop_id = Integer.parseInt(partes[0]); 
				int to_stop_id = Integer.parseInt(partes[1]);
				int transfer_type = Integer.parseInt(partes[2]);

				String min_transfer_time = "";

				if(partes.length > 3)
				{
					min_transfer_time = partes[3]; 

				}
				TransferVO transfer = new TransferVO(from_stop_id, to_stop_id, transfer_type, min_transfer_time);


				listaTransfers.add(transfer);




				linea = lector.readLine();
			}


		}catch (Exception e) {

			e.printStackTrace();
		}finally {
			if(lector != null)
			{
				try {
					lector.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}


	/*
	 * 
	 * 					M�todos
	 * 
	 */

	//A la hora de ordenar cambiar ruta trabaja por false!!


	// 
	public DoubleLinkedList<Ruta> darRutasAgencia(String fecha, String pAgencia)
	{
		DoubleLinkedList<Ruta>rutasAgencia = new DoubleLinkedList<>();


		darTripsFecha(fecha); 

		for(Ruta ruta: listaRutas)
		{
			if(ruta.getAgencyId().equals(pAgencia)&& ruta.getRutaTrabaja())
			{
				rutasAgencia.add(ruta);
			}
		}

		return rutasAgencia;
	}



	public DoubleLinkedList<Trip> darTripsConStopTimesyStops(String fecha, String idRuta)
	{



		Ruta ruta = buscarRuta(idRuta);
		DoubleLinkedList<Trip> tripsRuta = ruta.getListaTrips();

		joinTripAStopTimes(tripsRuta);
		joinTripsAStops(tripsRuta);




		return tripsRuta;
	}








	//Los trips en esta lista tienen sus stoptimes adentro
	public void joinTripAStopTimes(DoubleLinkedList<Trip> pLista)
	{

		DoubleLinkedList<Nodo<StopTimesVO>> bookmarks = listaStopTimes.darBookmarks(); 


		for (Trip trip : pLista) {

			boolean encontrado = false;
			Nodo<Nodo<StopTimesVO>> nodoHolder  = bookmarks.cabeza();

			Nodo<StopTimesVO> nodo = null;


			while(nodoHolder != null && !encontrado)
			{
				if(nodoHolder.darElemento().darElemento().getTripId() == trip.getTripId())
				{
					encontrado = true;
					nodo = nodoHolder.darElemento();
				}

				nodoHolder = nodoHolder.darSiguiente();
			}


			encontrado = false;
			while(nodo != null && !encontrado)
			{
				StopTimesVO stopTime = nodo.darElemento();

				if(stopTime.getTripId() == trip.getTripId())
				{
					trip.addStopTimes(stopTime);
				}
				else
				{
					encontrado = true;
				}

				nodo = nodo.darSiguiente(); 
			}

		}

	}


	//Pre: ya hizo join con stoptimes.
	public void joinTripsAStops(DoubleLinkedList<Trip> pLista)
	{

		for (Trip trip : pLista) {
			DoubleLinkedList<StopTimesVO> stopTimes = trip.darStopTimes();

			for (StopVO stop : listaStops) {

				for (StopTimesVO stopTimesVO : stopTimes) {

					if(stop.id() == stopTimesVO.getStopId())
					{
						trip.addStops(stop);
						stopTimesVO.setStop(stop);
					}

				}	
			}
		}


	}


	public DoubleLinkedList<TransferVO> darTransbordosRuta(String idRuta) {

		Ruta ruta = buscarRuta(idRuta);


		DoubleLinkedList<Trip> tripsRuta = ruta.getListaTrips();

		joinTripAStopTimes(tripsRuta);
		joinTripsAStops(tripsRuta);

		ruta.crearListaDeStopTimes();

		DoubleLinkedList<StopTimesVO> stoptimes = ruta.getListaStopTimes();

		DoubleLinkedList<TransferVO> transfers = new DoubleLinkedList<>();

		//coge el trasnfer a la primera parada que lo tenga
		for (StopTimesVO stopTimesVO : stoptimes ) {

			StopVO stop = stopTimesVO.getStop();

			transfers = darTransfer(stop);
			
			if(!transfers.estaVacio())
			{	
				break;
			}

		}

		
		int tripId = 0;
		boolean encontro = false;
		
		
		//System.out.println("Tama�o transfers" + transfers.getSize());
		for (TransferVO transfer : transfers)
		{
			System.out.println("StopId del transfer" + transfer.getToStopId());
			Nodo<StopTimesVO> nodoST = listaStopTimes.cabeza();
			while (nodoST != null) 
			 {
				StopTimesVO stopTime = nodoST.darElemento();
				//encuentra el primero
				
				if(stopTime.getStopId() == transfer.getToStopId())
				{
				
					tripId = stopTime.getTripId();
					encontro = true;
					transfer.addStopTimes(stopTime);
					
					System.out.println("Trip ID " + tripId);
					
				}else if ( tripId == stopTime.getTripId() && encontro)
				{
			
					transfer.addStopTimes(stopTime);
					nodoST = nodoST.darSiguiente(); 
					continue; 
				}else if( encontro){
					
					break ; 
				}
					
				
				nodoST = nodoST.darSiguiente();
			}
			
			
		}
		


		
		return transfers; 

	}




	public DoubleLinkedList<StopVO> buscarStops(DoubleLinkedList<String> pStops)
	{
		
		DoubleLinkedList<StopVO> stops = new DoubleLinkedList<>(); 
		
		for (String sStop : pStops) {
			
			StopVO stop = buscarStop(sStop);
			
			if(stop  != null)
			{
				stops.add(stop);
			}
			
		}
		
		return stops;
	}
	
	
	public StopVO buscarStop(String stopid)
	{
		
		int id = Integer.parseInt(stopid);
		for (StopVO stop : listaStops) {
			
			if(id == stop.id())
			{
				return stop; 
			}
		}
		
		return null; 
		
	}
	
	
	public void joinTodasLasRutas()
	{
		
		for (Ruta ruta : listaRutas) 
		{
			DoubleLinkedList<Trip> trips = ruta.getListaTrips();
			
			joinTripAStopTimes(trips);
			joinTripsAStops(trips);
		
		}
		
	}
	
	
	public DoubleLinkedList<TransferVO> darTransfer(StopVO stop)
	{

		
		Nodo<TransferVO> nodoT = listaTransfers.cabeza();
		
		boolean encontro = false;
		
		DoubleLinkedList<TransferVO> transfers = new DoubleLinkedList<>();
		
		while(nodoT != null )
		{
			
			TransferVO transfer = nodoT.darElemento();
		
			
			if(transfer.getFromStopId() == stop.id() )
			{
				
				
				transfer.setFromStop(stop);
				transfers.add(transfer); 
				
				encontro = true;
				
			}else if(encontro)
			{
				break;
				
			}
		
			
			
			nodoT = nodoT.darSiguiente();
		}


		for (TransferVO transfer : transfers) {
			

			System.out.println("TransferTime" + transfer.getMinTransferTime());
				
			}
			
			
			
		

		
		return transfers; 
}



public DoubleLinkedList<Trip>darTripsFecha(String fecha)
{
	DoubleLinkedList<Trip> listaTripsFecha = new DoubleLinkedList<>(); 

	String a�o = fecha.substring(0,3);
	String mes = fecha.substring(4,5);
	String dia = fecha.substring(6,7);
	String nuevaFecha = dia +"/" + mes + "/" + a�o;

	Date date = null;
	try {
		date = new SimpleDateFormat("dd/M/yyyy").parse(nuevaFecha);
	} catch (ParseException e) {
		e.printStackTrace();
	}


	Calendar c = Calendar.getInstance(); 
	c.setTime(date);

	int dayOftheWeek = c.get(Calendar.DAY_OF_WEEK);

	int service = 0;



	//mira que servicio lo presta dependiendo del dia de la seman
	if(dayOftheWeek == 2|dayOftheWeek == 3|dayOftheWeek == 4|dayOftheWeek == 5|dayOftheWeek == 6|dayOftheWeek == 7)
	{
		service = 1;
	}
	else if(dayOftheWeek == 1 )
	{
		service = 2;
	}
	else if (dayOftheWeek == 8)
	{
		service = 3;
	}

	//Mira si tiene excepci�n
	for (CalendarDateVO calendar : listaCalendarDates) {
		if(calendar.getDate().equals(fecha) && calendar.getServiceId() == service)
		{
			service = 0;
		}
	}


	if(service != 0)
	{

		for (Trip trip : listaTrips) {

			if(trip.getServiceId()== service)
			{
				trip.cambiarRutaTrabaja();
				listaTripsFecha.add(trip);
			}

		}

	}

	return listaTripsFecha;
}


public Ruta buscarRuta(String idRuta)
{
	for (Ruta ruta : listaRutas) {

		if(ruta.darId() == Integer.parseInt(idRuta))
		{
			return ruta;
		}

	}
	return null;
}

public DoubleLinkedList<Ruta> darListaRutas()
{	
	return listaRutas;
}



public DoubleLinkedList<StopVO> darListaStops()
{
	return listaStops;
}


public DoubleLinkedList<Trip> darListaTrips()
{
	return listaTrips;
}

public DoubleLinkedList<StopTimesVO> darListaStopTimes()
{
	return listaStopTimes;
}

public DoubleLinkedList<CalendarVO> darListaCalendar()
{
	return listaCalendario;
}

public DoubleLinkedList<TransferVO> darListaTransferencias()
{
	return listaTransfers;
}


public Trip buscarTrip(String pid)
{
	int id = Integer.parseInt(pid); 
	
	for (Trip trip : listaTrips) {
		
		if(trip.getTripId() == id)
		{
			return trip;
		}
		
	}
	
	return null;
	
}

public DoubleLinkedList<Trip> tripConStops(String idViaje) {
	
	buscarTrip(idViaje);
	
	DoubleLinkedList<Trip> listaContrip = new DoubleLinkedList<>(); 
	
	return listaContrip;
	
	
	
}




}
