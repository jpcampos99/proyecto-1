package model.logic;

import java.io.File;
import java.util.concurrent.SynchronousQueue;

import model.data_structures.DoubleLinkedList;
import model.data_structures.LinkedQueue;
import model.data_structures.Nodo;
import model.vo.BusUpdateVO;
import model.vo.RetardoVO;
import model.vo.ServiceVO;
import model.vo.StopEstimateVO;
import model.vo.StopTimesVO;
import model.vo.StopVO;
import model.vo.TransferVO;
import api.ISTSManager;

public class STSManager implements ISTSManager{


	private LinkedQueue<BusUpdateVO> cola;
	private Estatico estaticoManager;
	private TiempoReal tiempoRealManager;
	private OrdenadorListas ordenador;
	
	private String fechaCargada;
	
	public STSManager() {

		ordenador = new OrdenadorListas(); 
	
	}
	
	
	@Override
	public void ITScargarGTFS() {

		estaticoManager = new Estatico();
		estaticoManager.cargarCalendarDate(); 
		estaticoManager.cargarCalendario();
		estaticoManager.cargarRutas();
		estaticoManager.cargarStopTimes();
		estaticoManager.cargarStops();
		estaticoManager.cargarTrips();
		estaticoManager.cargarTransfers();
		
		
		//Se demor� un minuto
		//estaticoManager.joinTodasLasRutas();
		
		
	}

	@Override
	public void ITScargarTR(String fecha) {

			
		
		
		
		String pathBus = "./data/realtime/BusServices";
		String pathStop = "./data/realtime/StopEstimates";
		
		if(fecha.equals("2017822"))
		{
			fechaCargada = fecha; 
			pathBus = pathBus + "/RealTime-8-22-BUSES_SERVICE";
			pathStop = pathStop + "/RealTime-8-22-STOPS_ESTIM_SERVICES";
		}
		else if(fecha.equals("2017821"))
		{	fechaCargada = fecha;
			pathBus = pathBus + "/RealTime-8-21-BUSES_SERVICE";
			pathStop = pathStop + "/RealTime-8-21-STOPS_ESTIM_SERVICES";
		}else
		{
			System.out.println("lo siento broder, no hay data para eso");
			return;
		}
		
		
		
			
		File f2 = new File(pathStop);
		File[] updateFiles2 = f2.listFiles();
		tiempoRealManager = new TiempoReal();
		for (int i = 0; i < updateFiles2.length; i++) {
			tiempoRealManager.cargarStopEstimates(updateFiles2[i]);
		}
		
//		DoubleLinkedList<StopEstimateVO> listaStopE = tiempoRealManager.darListaStopEstimates();
//		
//		for (StopEstimateVO stopEstimateVO : listaStopE) {
//			
//			System.out.println(stopEstimateVO.getStopCode());
//		}
	
		
		
		File f = new File(pathBus);
		File[] updateFiles = f.listFiles();
		
		for (int i = 0; i < updateFiles.length; i++) {
			tiempoRealManager.cargarBusUpdates(updateFiles[i]);
		}
		
		
	
		

		
	}
	

	

	@Override
	public DoubleLinkedList<Ruta> ITSrutasPorEmpresa(String nombreEmpresa, String fecha) {

	
		//esta lista no tiene size ni cola
		DoubleLinkedList<Ruta> lista = ordenador.ordenarPorMerge(estaticoManager.darRutasAgencia(fecha, nombreEmpresa));
	
		
	
		
		return lista; 
		
	}

	@Override
	public DoubleLinkedList<Trip> ITSviajesRetrasadosRuta(String idRuta, String fecha) {

		if(!fecha.equals(fechaCargada))
		{
			System.out.println("No se tienen datos cargados para esta fecha");
		}
		
		DoubleLinkedList<Trip> tripsConRetraso = estaticoManager.darTripsConStopTimesyStops(fecha, idRuta);
		
		
		tiempoRealManager.joinBusUpdatesATrips(tripsConRetraso);
		
		
		
		System.out.println("Hace join a Bus");

		
//		for (Trip trip : tripsConRetraso) {
//			
//			System.out.println("TRIP: " + trip.getTripId());
//			for (BusUpdateVO bus : trip.darBusUdpates()) {
//				
//				System.out.println(bus.darRecordedTime());
//			}
//			
//		}
		
		Nodo<Trip> nodoTrip = tripsConRetraso.cabeza();

		while (nodoTrip != null)
		{
			Trip trip = nodoTrip.darElemento();
			
			if(trip.getTiempoRetardo() == 0)
			{
				
				Nodo<Trip> nodoAEliminar = nodoTrip;
				nodoTrip = nodoTrip.darSiguiente();
				tripsConRetraso.eliminarNodo(nodoAEliminar);
			}else{
			
			nodoTrip = nodoTrip.darSiguiente();
			}
		}
		
		
		
		

		
		
		ordenador.ordernarInsertion(tripsConRetraso);
		
	
		
		
		return tripsConRetraso;
	}

	@Override
	public DoubleLinkedList<StopVO> ITSparadasRetrasadasFecha(String fecha) {

		if(!fecha.equals(fechaCargada))
		{
			System.out.println("No se tienen datos cargados para esta fecha");
		}
		
		DoubleLinkedList<StopVO> stops = estaticoManager.darListaStops();
		
		stops = ordenador.ordenarPorMerge(stops); 
		
		stops.nuevaCabezaPos(137);
		
		
	
		
		
		
		return ordenador.ordenarPorMergeIncidentes(tiempoRealManager.joinStopsStopEstimates(stops));
	}

	@Override
	public DoubleLinkedList<TransferVO> ITStransbordosRuta(String idRuta) {
				
		return ordenador.ordenarPorMerge(estaticoManager.darTransbordosRuta(idRuta));
	}

	
	public DoubleLinkedList<DoubleLinkedList<Trip>> ITSrutasPlanUtilizacion(String fecha, String horaInicio,
			String horaFin, DoubleLinkedList<String> secuenciaDeParadas) {
		
		
		DoubleLinkedList<DoubleLinkedList<Trip>> megaLista = new DoubleLinkedList<>(); 
		
		estaticoManager.buscarStops(secuenciaDeParadas);
		
		
		return megaLista; 
	}
	
	

	public DoubleLinkedList<Ruta> ITSrutasPorEmpresaParadas( String nombreEmpresa, String fecha) 
	{
		return ordenador.ordernarPorNumeroParadas(estaticoManager.darRutasAgencia(fecha, nombreEmpresa)); 

		// TODO Auto-generated method stub
	}

	@Override
	public DoubleLinkedList<Trip> ITSviajesRetrasoTotalRuta(String idRuta,String fecha) 
	{
		//identificar	 todos	 los	 viajes	 en	 los	 que	 despu�s	 de	 un	
		//retardo,	 todas	 las	 paradas	 siguientes	 tuvieron	 retardo.	 
		//Se	 debe	 retornar	 una	 lista	ordenada	por	el	id	del	viaje,	
		//y	la	localizaci�n	de	las	paradas,	de	mayor	a	menor	en	tiempo	
		//de	retardo.

		Ruta ruta1 = estaticoManager.buscarRuta(idRuta); 

		DoubleLinkedList<Trip> ruta1trips = ruta1.getListaTrips(); 

		estaticoManager.joinTripAStopTimes( ruta1trips );
		estaticoManager.joinTripsAStops( ruta1trips );
		tiempoRealManager.joinBusUpdatesATrips( ruta1trips ); 

		Nodo<Trip> nodoTrip = ruta1trips.cabeza(); 

		while ( nodoTrip != null )
		{
			Trip trip = nodoTrip.darElemento();

			DoubleLinkedList<Trip> trips = ITSviajesRetrasadosRuta(idRuta, fecha);

			if( trip.tieneRetardos() )
			{

			}

			if( trip.getTiempoRetardo() == 0 )
			{

			}
			else
			{
				nodoTrip = nodoTrip.darSiguiente();
			}
		}

		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DoubleLinkedList<String> ITSretardoHoraRuta(String idRuta,String Fecha) 
	{
		//Identificar	la	franja	de	hora	entera	(ejemplo:	2:00pm	a	
		//3:00	pm	o	5:00	pm	a	6:00	pm)	en	las	 que	m�s	 retardos	
		//hubo,	 para	 una	 ruta	 determinada	en	 una	 fecha	 dada.	
		//Se	 debe	retornar	una	lista	con	la	hora de	m�s	retardos,	
		//ordenada	por	tiempo	total	de	retardo.

		DoubleLinkedList<String> listaRetardos = new DoubleLinkedList<>(); 

		Ruta ruta1 = estaticoManager.buscarRuta(idRuta); 

		DoubleLinkedList<Trip> ruta1trips = ruta1.getListaTrips(); 
		DoubleLinkedList<RetardoVO> retardos = new DoubleLinkedList<>(); 
		int tiempoTotalRetardo = 0; 
		String rangohoras = " "; 

		estaticoManager.joinTripAStopTimes( ruta1trips );
		tiempoRealManager.joinBusUpdatesATrips(ruta1trips); 

		Nodo<Trip> nodoTrip = ruta1trips.cabeza(); 

		if( nodoTrip != null )
		{
			Trip trip = nodoTrip.darElemento();

			if( trip.tieneRetardos() )
			{			
				DoubleLinkedList<StopTimesVO> stopstim = trip.darStopTimes(); 

				Nodo<StopTimesVO> nodoStopTimes = stopstim.cabeza(); 

				if( nodoStopTimes != null)
				{
					StopTimesVO stopT = nodoStopTimes.darElemento(); 
					String horaLlegada = stopT.getArrivalTime();	

					Nodo<RetardoVO> nodoretrado = retardos.cabeza(); 
					int retraso = nodoretrado.darElemento().getTiempoDeRetraso(); 
					StopTimesVO stop1 = nodoretrado.darElemento().getStopTime();

					if( stopT.equals(stop1) )
					{						
						String hora1 = horaLlegada; 
						String[] partes = hora1.split(":");
						int Hora = Integer.parseInt(partes[1]);
						int Minutos = Integer.parseInt(partes[2]);
						int Segundos = Integer.parseInt(partes[3]);

						int horaInicio = Hora; 

						for(int i = horaInicio; i < 24; i++)
						{
							int horaFin = horaInicio + 1; 

							DoubleLinkedList<StopVO> stops = trip.darStops(); 

							Nodo<StopVO> nodoStop = stops.cabeza(); 

							StopVO stop = nodoStop.darElemento(); 

							if( stop.id() == stopT.getStopId() )
							{
								String stopArrivaltime = stopT.getArrivalTime(); 

								int stopArrivalTime = Integer.parseInt(stopArrivaltime) + 1; 

								if( stopArrivalTime >= i && stopArrivalTime < i + 1 )
								{
									tiempoTotalRetardo = tiempoTotalRetardo + retraso;
									rangohoras = i+"-"+ i +1; 
								}

								nodoStop = nodoStop.darSiguiente(); 
							}						

							//String HoraI = String.valueOf(horaInicio); 
							//HoraI = Integer.toString(horaInicio); 

							//String HoraF = String.valueOf(horaFin); 
							//HoraF = Integer.toString(horaFin); 

							//ManagerHoras manejador = new ManagerHoras(); 
							//int valor1 = manejador.compararHoras(horaLlegada, HoraI); 
							//int valor2 = manejador.compararHoras(horaLlegada, HoraF); 

						}

						listaRetardos.add(rangohoras + tiempoTotalRetardo);
					}

					nodoStopTimes = nodoStopTimes.darSiguiente(); 
				}	
			}

			nodoTrip = nodoTrip.darSiguiente(); 
			// TODO Auto-generated method stub

		}
		return listaRetardos;
	}

	@Override
	public DoubleLinkedList<Trip> ITSbuscarViajesParadas(String idOrigen,
			String idDestino, String fecha, String horaInicio, String horaFin) 
			{

		//Dados	los	ids	de	una	parada	de	origen	y	una	parada	destino	
		//buscar	los	viajes	de	rutas de bus	para ir	del	origen	
		//al	destino	(sin	trasbordos)	en	una	fecha	y	franja	de	
		//horario	(hora entera	inicial	y	hora	entera	 final)	dadas.
		//Por	ejemplo,	buscar	los	viajes	de	 rutas	entre	la	
		//parada	11940	y	la	parada	9224	en	la	 franja	12:00pm	
		//� 2:00	pm. Para	los	viajes	de	rutas que	sean	soluci�n
		//al	problema	se	debe	informar	el	Id	de	la	ruta,	
		//el	Id	del	viaje,	el	tiempo del	viaje	en	la	parada	origen
		//y	el	tiempo	del	viaje	en	la	parada	destino

		DoubleLinkedList rutasCumple = new DoubleLinkedList( ); 

		DoubleLinkedList<Ruta> rutas = estaticoManager.darListaRutas(); 
		Nodo<Ruta> nodoRuta = rutas.cabeza(); 

		int idOrige = Integer.parseInt(idOrigen); 
		int idDestin = Integer.parseInt(idDestino); 
		StopVO origen = null; 
		StopVO destino = null; 
		int identificadorTrip = 0; 

		while (nodoRuta != null)
		{
			Ruta ruta = nodoRuta.darElemento(); 

			if( ruta.getListaTrips() != null )
			{
				DoubleLinkedList<Trip> trips = estaticoManager.darListaTrips(); 

				estaticoManager.joinTripsAStops(trips);

				Nodo<Trip> nodotrip = trips.cabeza(); 

				if( nodotrip != null)
				{
					Trip trip = nodotrip.darElemento();

					DoubleLinkedList<StopVO> stops = trip.darStops(); 

					Nodo<StopVO> nodoStop = stops.cabeza(); 

					if( nodoStop != null)
					{
						StopVO stop = nodoStop.darElemento(); 

						if( stop.id() == idOrige )
						{
							origen = stop; 
						}
						if( stop.id() == idDestin)
						{
							destino = stop; 
						}

						if( origen != null && destino != null)
						{
							identificadorTrip = trip.getTripId(); 
						}

						DoubleLinkedList<StopTimesVO> stopTimes = trip.darStopTimes(); 
						Nodo<StopTimesVO> nodoStopTimes = stopTimes.cabeza(); 

						if( nodoStopTimes != null )
						{
							StopTimesVO stopTim = nodoStopTimes.darElemento(); 

							if( stopTim.getTripId() == identificadorTrip )
							{
								String arrivaltime = stopTim.getArrivalTime(); 
								ManagerHoras manejador = new ManagerHoras(); 

								int valor1 = manejador.compararHoras(arrivaltime, horaInicio); 
								int valor2 = manejador.compararHoras(arrivaltime, horaFin); 

								if( valor1 > 0 && valor2 < 0)
								{
									//1 si pHora es m�s tarde que pHora2, -1 si es m�s temprano
									rutasCumple.add(ruta.darId()+ trip.getTripId() );
								}
							}

							nodoStopTimes = nodoStopTimes.darSiguiente(); 
						}

						nodotrip = nodotrip.darSiguiente();
					}
					else
					{
						nodotrip = nodotrip.darSiguiente();
					}
				}
			}

			nodoRuta = nodoRuta.darSiguiente(); 
		}
		// TODO Auto-generated method stub
		return rutasCumple;
			}

	@Override
	public Ruta ITSrutaMenorRetardo(String idRuta, String fecha) 
	{
		//Dar	la	ruta	que	tiene	menos	retardo	promedio	en	distancia
		//de	sus	viajes en	una	fecha	dada.	El	retardo	promedio	en	
		//distancia	de	un	viaje	corresponde	al	total	de	los	tiempos	de	
		//retardo	de	sus	paradas	dividido	por	la	distancia	recorrida.
		//Por	ejemplo,	si	los	tiempos	de	retardo	de	las	paradas	del	viaje
		//3492	es	1200	seg	y	recorre	20	Km,	el	retardo	promedio	en	
		//distancia	del	viaje	3492	es	de	60	seg	por	kil�metro.	

		DoubleLinkedList<Ruta> rutas = estaticoManager.darListaRutas(); 

		double retardoProm = 0; 
		double menosRetardorpom = 0; 
		Ruta rutamenosProm = null; 
		double tiempoRetardo = 0.0; 

		Nodo<Ruta> nodoRuta = rutas.cabeza(); 

		while (nodoRuta != null)
		{
			Ruta ruta = nodoRuta.darElemento(); 
			menosRetardorpom = retardoProm; 

			if( ruta.getListaTrips() != null )
			{
				DoubleLinkedList<Trip> trips = estaticoManager.darListaTrips(); 

				estaticoManager.joinTripAStopTimes(trips);
				estaticoManager.joinTripsAStops(trips);

				Nodo<Trip> nodotrip = trips.cabeza(); 

				if( nodotrip != null)
				{
					Trip trip = nodotrip.darElemento();

					tiempoRetardo = tiempoRetardo + trip.getTiempoRetardo(); 
					Double distancia = 0.0; 

					if( trip.getTiempoRetardo() != 0 )
					{
						DoubleLinkedList<StopTimesVO> stopt = trip.darStopTimes(); 

						Nodo<StopTimesVO> nosostop = stopt.cola(); 

						if( nosostop != null)
						{
							StopTimesVO stop = nosostop.darElemento();
							distancia = distancia + stop.getDistTraveled();
						}							

						nodotrip = nodotrip.darSiguiente();
					}
					else
					{
						nodotrip = nodotrip.darSiguiente();
					}

					retardoProm = tiempoRetardo/distancia; 
				}
			}

			if( menosRetardorpom > retardoProm  )
			{
				menosRetardorpom = retardoProm; 
				rutamenosProm = ruta; 
			}

			nodoRuta = nodoRuta.darSiguiente();
		}

		// TODO Auto-generated method stub
		return rutamenosProm;
	}

	@Override
	public Void ITSInit() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DoubleLinkedList<ServiceVO> ITSserviciosMayorDistancia(String fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DoubleLinkedList<RetardoVO> ITSretardosViaje(String fecha,
			String idViaje) {

		DoubleLinkedList<Trip> trip = estaticoManager.tripConStops(idViaje);
		
		tiempoRealManager.joinBusUpdatesATrips(trip);
		
		return trip.cabeza().darElemento().darRetardos();
	}

	@Override
	public DoubleLinkedList<StopVO> ITSparadasCompartidas(String fecha) {

		tiempoRealManager.darParadasCompartidas();
		
		return null;
	}


	
	

}
