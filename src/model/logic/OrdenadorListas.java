package model.logic;

import model.data_structures.DoubleLinkedList;
import model.data_structures.Nodo;
import model.vo.BusUpdateVO;
import model.vo.CalendarDateVO;
import model.vo.StopVO;

public class OrdenadorListas <T extends Comparable<T>>{

	Nodo <T> sorted;
	Nodo <T> head;


	public DoubleLinkedList<Ruta> ordernarPorNumeroParadas(DoubleLinkedList<Ruta> plista)
	{
		for (Nodo<Ruta> nodoi = plista.cabeza().darSiguiente(); nodoi != null; nodoi = nodoi.darSiguiente()) 
		{
			for (Nodo<Ruta> nodoj = nodoi; nodoj != null && nodoj != plista.cabeza(); nodoj = nodoj.darAnterior())
			{
				Ruta ruta1 = nodoj.darElemento();
				Nodo<Ruta> nodojmenos = nodoj.darAnterior();
				
				Ruta rutajmenos = nodojmenos.darElemento();
				
				if( ruta1.getListaStops().getSize() < rutajmenos.getListaStops().getSize() )
				{
					System.out.println("intercambia");
					plista.intercambiar(nodojmenos, nodoj);
				}			
			}
		}
		
		return null;
	}

	public Nodo<T> mergeSort(Nodo<T> head) {
		// Base case : if head is null
        if (head == null || head.darSiguiente() == null)
        {
            return head;
        }
 
        // get the middle of the list
        Nodo<T> middle = getMiddle(head);
        
        Nodo<T> nextofmiddle = middle.darSiguiente();
        
//        System.out.println("El elemento de la mitad es" + nextofmiddle.darElemento().toString());
//        
//        System.out.println("El ultimo de la otra lista es" + middle.darElemento().toString());
        // set the next of middle node to null
        middle.cambiarSiguiente(null);
        nextofmiddle.cambiarAnterior(null);
 
        // Apply mergeSort on left list
        Nodo<T> left = mergeSort(head);
 
        // Apply mergeSort on right list
        
        Nodo<T> right = mergeSort(nextofmiddle);
 
        // Merge the left and right lists
        Nodo<T> sortedlist = sortedMerge(left, right);
        return sortedlist;
	}

	//Merge subroutine to merge two sorted lists
	public Nodo<T> sortedMerge(Nodo<T> a, Nodo<T> b) {
		 Nodo<T> result = null;
	        /* Base cases */
	        if (a == null)
	            return b;
	        if (b == null)
	            return a;
	 
	        /* Pick either a or b, and recur */
	        if (a.darElemento().compareTo(b.darElemento()) <= 0) 
	        {	
	            result = a;
	            result.cambiarSiguiente(sortedMerge(a.darSiguiente(), b)); 
	        } 
	        else
	        {
	            result = b;
	            result.cambiarSiguiente(sortedMerge(a, b.darSiguiente()));
	        }
	        return result;
	}

	//Finding the middle element of the list for splitting
	public Nodo<T> getMiddle(Nodo<T> head) {
		//Base case
        if (head == null)
        { 
        	return head; 
        
        }
        Nodo<T> fastptr = head.darSiguiente();
        Nodo<T> slowptr = head;
       
        
        // Move fastptr by two and slow ptr by one
        // Finally slowptr will point to middle node
        while (fastptr != null)
        	
        {	
        	
            fastptr = fastptr.darSiguiente();
            
            if(fastptr!=null)
            {
                slowptr = slowptr.darSiguiente();
                fastptr=fastptr.darSiguiente();
            }
        }
        
        return slowptr;
	}



	public DoubleLinkedList<T> ordenarPorMerge(DoubleLinkedList<T> pLista)
	{
		DoubleLinkedList<T> listaOrdenada = new DoubleLinkedList<>();
		
		listaOrdenada.setCabeza(mergeSort(pLista.cabeza()));
		
		return listaOrdenada;

	}
	
	public Nodo<T> cabezaMerge(DoubleLinkedList<T> pLista)
	{
		return mergeSort(pLista.cabeza()); 
	}
	


	//Que elimine las repeticiones?
	public void ordernarInsertion(DoubleLinkedList<T> plista)
	{

		for (Nodo<T> nodoi = plista.cabeza().darSiguiente(); nodoi != null; nodoi = nodoi.darSiguiente()) {


			for (Nodo<T> nodoj = nodoi; nodoj != null && nodoj != plista.cabeza() && nodoj.darAnterior() != null; nodoj = nodoj.darAnterior())
			{

				T j = nodoj.darElemento();
				Nodo<T> nodojmenos = nodoj.darAnterior();



				T jmenos = nodojmenos.darElemento();


				if(j.compareTo(jmenos) < 0)
				{
					//System.out.println("intercambia");
					plista.intercambiar(nodojmenos, nodoj);
					nodoj = nodojmenos;
				}

				if(j.compareTo(jmenos) == 0)
				{
					//System.out.println("Elimina");
					//plista.eliminarNodo(nodojmenos);
				}



			}


		}



	}



	public DoubleLinkedList<StopVO> ordenarPorMergeIncidentes(DoubleLinkedList<StopVO> pLista) {


		DoubleLinkedList<StopVO> listaOrdenada = new DoubleLinkedList<>();
		
		listaOrdenada.setCabeza(mergeSortStop(pLista.cabeza()));
		
		return listaOrdenada;
		
		
	}


	public Nodo<StopVO> mergeSortStop(Nodo<StopVO> head) {
		// Base case : if head is null
        if (head == null || head.darSiguiente() == null)
        {
            return head;
        }
 
        // get the middle of the list
        Nodo<StopVO> middle = getMiddleStop(head);
        
        Nodo<StopVO> nextofmiddle = middle.darSiguiente();
        
//        System.out.println("El elemento de la mitad es" + nextofmiddle.darElemento().toString());
//        
//        System.out.println("El ultimo de la otra lista es" + middle.darElemento().toString());
        // set the next of middle node to null
        middle.cambiarSiguiente(null);
        nextofmiddle.cambiarAnterior(null);
 
        // Apply mergeSort on left list
        Nodo<StopVO> left = mergeSortStop(head);
 
        // Apply mergeSort on right list
        
        Nodo<StopVO> right = mergeSortStop(nextofmiddle);
 
        // Merge the left and right lists
        Nodo<StopVO> sortedlist = sortedMergeStop(left, right);
        return sortedlist;
	}

	//Merge subroutine to merge two sorted lists
	public Nodo<StopVO> sortedMergeStop(Nodo<StopVO> a, Nodo<StopVO> b) {
		 Nodo<StopVO> result = null;
	        /* Base cases */
	        if (a == null)
	            return b;
	        if (b == null)
	            return a;
	 
	        /* Pick either a or b, and recur */
	        if (a.darElemento().compareNumeroIncidentes(b.darElemento()) <= 0) 
	        {	
	            result = a;
	            result.cambiarSiguiente(sortedMergeStop(a.darSiguiente(), b)); 
	        } 
	        else
	        {
	            result = b;
	            result.cambiarSiguiente(sortedMergeStop(a, b.darSiguiente()));
	        }
	        return result;
	}

	//Finding the middle element of the list for splitting
	public Nodo<StopVO> getMiddleStop(Nodo<StopVO> head) {
		//Base case
        if (head == null)
        { 
        	return head; 
        
        }
        Nodo<StopVO> fastptr = head.darSiguiente();
        Nodo<StopVO> slowptr = head;
       
        
        // Move fastptr by two and slow ptr by one
        // Finally slowptr will point to middle node
        while (fastptr != null)
        	
        {	
        	
            fastptr = fastptr.darSiguiente();
            
            if(fastptr!=null)
            {
                slowptr = slowptr.darSiguiente();
                fastptr=fastptr.darSiguiente();
            }
        }
        
        return slowptr;
	}



}
