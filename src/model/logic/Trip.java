package model.logic;

import model.data_structures.DoubleLinkedList;
import model.data_structures.Nodo;
import model.vo.BusUpdateVO;
import model.vo.RetardoVO;
import model.vo.StopTimesVO;
import model.vo.StopVO;

public class Trip implements Comparable<Trip> {


	private int routeId;
	private int serviceId;
	private int tripId;
	private String tripHeadsign;
	private String tripShortName;
	private int directionId;
	private int shapeId;
	private boolean wheelchairAccesible;
	private boolean bikesAllowed;
	private Ruta ruta;
	
	private int tiempoRetardo;

	private DoubleLinkedList<StopTimesVO> stopTimes;
	private DoubleLinkedList<StopVO> stops;

	private DoubleLinkedList<BusUpdateVO> busUpdates;
	private DoubleLinkedList<RetardoVO> retardos; 
	
	private ManagerHoras horas;

	public static final double VELOCIDAD = 11.111; 


	public Trip( int prouteId, int pserviceId, int ptripId, String ptripHeadsing, String ptripShortName, int pdirectionId, int pshapeId, boolean pwheelchairaccesible, boolean pbikes ) {

		
		stopTimes = new DoubleLinkedList<>();
		stops = new DoubleLinkedList<>();
		busUpdates = new DoubleLinkedList<>();
		retardos = new DoubleLinkedList<>();

		horas = new ManagerHoras(); 
		ruta = null; 
		
		setTiempoRetardo(0);
		
		setRouteId(prouteId);
		setServiceId(pserviceId);
		setTripId(ptripId); 
		tripHeadsign = ptripHeadsing;
		setTripShortName(ptripShortName);
		setDirectionId(pdirectionId); 
		shapeId = pshapeId; 
		wheelchairAccesible = pwheelchairaccesible; 
		bikesAllowed = pbikes;


	}


	public DoubleLinkedList<RetardoVO> darRetardos()
	{
		return retardos;
	}
	
	/*
	 * Compara la distancia entre dos busUp con el st para saber si esta antes, despu�s o en medio de los dos busUpdates
	 * Si esta antes o en la mitad, compara  el ST con el primer busUP y pasa al sigueinte ST
	 * Si est� despu�s compara y pasa al siguiente busUpdae
	 * 
	 * Al comparar para hacer el retraso usa la distancia, el tiempo que recorrer�a a velocidad de 40km y el tiempo de diferencia entre las horas
	 */
	public void crearRetrasos()
	{


		Nodo<StopTimesVO> nodoST = stopTimes.cabeza();
	
		Nodo<BusUpdateVO> nodoBus = busUpdates.cabeza();

		loopST:
		while(nodoST != null)
		{

			StopTimesVO stopTime = nodoST.darElemento();
			String horaST =  stopTime.getArrivalTime(); 
			
			
			while(nodoBus!=null)
			{
				BusUpdateVO bus = nodoBus.darElemento();
				String horaBus = bus.darRecordedTime();
				horaBus = horas.cambiarA24horas(horaBus);
				
				double tiempo = 0;
				
				//si el �ltimo busupdate lo compara con la stop y avanza en Stoptimes
				if(nodoBus.darSiguiente() == null)
				{
					tiempo = calcularTiempo(bus,stopTime);
					int diferenciaHoras = horas.difereciaDosHoras(horaBus, horaST);
					hayRetraso(tiempo, diferenciaHoras, stopTime); 
					nodoST = nodoST.darSiguiente();
					continue loopST;
				}
				
				BusUpdateVO siguienteBus = nodoBus.darSiguiente().darElemento();
				
				//Si siguiente busUP es el mismo, avance en busUpdates
				if(bus.equals(siguienteBus))
				{
					nodoBus = nodoBus.darSiguiente();
					continue;
				}
				
				//mira si el stop est� antes, despu�s o en el medio de los dos busupdates
				int ubicacionStop = ubicacionStop(bus, siguienteBus, stopTime) ; 

				
				String horaSiguienteBus = siguienteBus.darRecordedTime();
				horaSiguienteBus = horas.cambiarA24horas(horaSiguienteBus); 
				
				
				// si est� en la mitad o antes, avanaz en st
				if(ubicacionStop == 0 || ubicacionStop == 1)
				{
					tiempo = calcularTiempo(bus,stopTime);
					int diferenciaHoras = horas.difereciaDosHoras(horaBus, horaST);
					
					hayRetraso(tiempo, diferenciaHoras, stopTime); 
					nodoBus = nodoBus.darSiguiente();
					continue;
					
					
				}else if(ubicacionStop == 1)
				{
					if(nodoBus.siguiente != null )
					{
						nodoBus = nodoBus.darSiguiente();
						continue;
						
					}else{
						tiempo = calcularTiempo(bus,stopTime);
						int diferenciaHoras = horas.difereciaDosHoras(horaBus, horaST);
						hayRetraso(tiempo, diferenciaHoras, stopTime); 
						
						
					}
					
					
				}
				
			


				nodoBus = nodoBus.darSiguiente();
			}

			
			nodoST = nodoST.darSiguiente();

		}





	}

	
	private boolean hayRetraso(double tiempo, int diferenciaHoras, StopTimesVO stopTime)
	{
		
		if(tiempo > diferenciaHoras)
		{
			int retraso = (int) (tiempo - diferenciaHoras);
			
			setTiempoRetardo(getTiempoRetardo() + retraso);
			
			RetardoVO retardo = new RetardoVO(this, retraso, stopTime);
			addRetardo(retardo);
			return true;
		}
		
		return false;
	}
	
	
	public Double calcularTiempo(BusUpdateVO bus, StopTimesVO stopT)
	{
		StopVO stop = stopT.getStop();
		
		double busLat = Double.parseDouble(bus.darLatitude());
		double busLon = Double.parseDouble(bus.darLongitude());
		
		double stopLat = stop.getStopLat();
		double stopLon = stop.getStopLon();
		double distancia = getDistance(busLat, busLon, stopLat, stopLon);
		
		return distancia / VELOCIDAD;
		
	}
	

	/*
	 * 0 si est� en la mitad de los updates
	 * 1 si est� despues de los dos updates
	 * -1 si est� antes de los dos uptades
	 */
	public int ubicacionStop(BusUpdateVO bus1, BusUpdateVO bus2, StopTimesVO stopT)
	{
		StopVO stop = stopT.getStop();
		
		double bus1Lat = Double.parseDouble(bus1.darLatitude());
		double bus1Lon = Double.parseDouble(bus1.darLongitude());
		
		double bus2Lat = Double.parseDouble(bus2.darLatitude());
		double bus2Lon = Double.parseDouble(bus2.darLongitude());
		
		double stopLat = stop.getStopLat();
		double stopLon = stop.getStopLon();
		
		double b1b2 = getDistance(bus1Lat, bus1Lon, bus2Lat, bus2Lon);
		
		double b1ST = getDistance(bus1Lat, bus1Lon, stopLat, stopLon);
		double b2ST = getDistance(bus2Lat, bus2Lon, stopLat, stopLon);
		
		
		//Est� en la mitad

		
		if(b1b2 > b1ST && b1b2 > b2ST)
		{
			return 0;
		}
		//Est� antes
		if(b2ST > b1ST && b2ST > b1b2)
		{
			return -1;
		}//Est� despu�s
		if(b1ST > b2ST && b1ST > b1b2 )
		{
			return 1;
		}
		
		
		
		
		return -2;
	}
	
	public double getDistance(double lat1, double lon1, double lat2, double lon2) 
	{
		// TODO Auto-generated method stub
		final int R = 6371*1000; // Radious of the earth

		Double latDistance = toRad(lat2-lat1);
		Double lonDistance = toRad(lon2-lon1);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
				Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
				Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;

		return distance;
	}
	
	
	private Double toRad(Double value) 
	{
		return value * Math.PI / 180;
	}
	
	


	public boolean tieneRetardos ()
	{

		return retardos.getSize() != 0;
	}


	public void addRetardo(RetardoVO retardo)
	{
		retardos.add(retardo);
	}

	public void addStopTimes(StopTimesVO stoptime)
	{
		stopTimes.addAtEnd(stoptime);
	}

	public void addStops(StopVO stop)
	{
		stops.addAtEnd(stop);
	}

	public void addABusUpdates(BusUpdateVO busUpdate)
	{
		busUpdates.add(busUpdate);

	}

	public DoubleLinkedList<BusUpdateVO> darBusUdpates()
	{
		return busUpdates;
	}

	public DoubleLinkedList<StopTimesVO> darStopTimes()
	{
		return stopTimes;
	}

	public DoubleLinkedList<StopVO> darStops()
	{
		return stops; 
	}

	public void cambiarRutaTrabaja()
	{
		ruta.setRutaTrabaja(true);
	}


	public int getRouteId() {
		return routeId;
	}



	public void setRouteId(int routeId) {
		this.routeId = routeId;
	}



	public int getTripId() {
		return tripId;
	}



	public void setTripId(int tripId) {
		this.tripId = tripId;
	}



	public String getTripShortName() {
		return tripShortName;
	}



	public void setTripShortName(String tripShortName) {
		this.tripShortName = tripShortName;
	}


	public String toString()
	{
		return "" +  routeId; 
	}


	public int getDirectionId() {
		return directionId;
	}


	public void setDirectionId(int directionId) {
		this.directionId = directionId;
	}



	public int getServiceId() {
		return serviceId;
	}



	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}




	public Ruta getRuta() {
		return ruta;
	}




	public void setRuta(Ruta ruta) {
		this.ruta = ruta;
	}


	@Override
	public int compareTo(Trip o) {
		
		if(tiempoRetardo > o.getTiempoRetardo())
		{
			return 1;
		}
		else if (tiempoRetardo < o.getTiempoRetardo())
		{
			return -1;
		}
		
		return 0;
	}


	public int getTiempoRetardo() {
		return tiempoRetardo;
	}


	public void setTiempoRetardo(int tiempoRetardo) {
		this.tiempoRetardo = tiempoRetardo;
	}






}
