package model.logic;

public class ManagerHoras {
	

	
	
	public String cambiarA24horas (String phora)
	{
		phora = phora.trim(); 
		String nuevaHora= ""; 

		if(	phora.endsWith("am"))
		{
			String sinAm = phora.split(" ")[0];
			return sinAm;
		}
		else if (phora.endsWith("pm"))
		{
			String[] partesHora = phora.split(":");
			int hora = Integer.parseInt(partesHora[0]);
			String sinPM = partesHora[2].split(" ")[0];
			
			hora += 12;
			nuevaHora = hora + ":" + partesHora[1] + ":" + sinPM;

		}

		return nuevaHora;
	}

	
	//Da las diferencia de phora1 - phora2 y un arreglo 
	public  int [] formatearHoras(String phora1, String phora2)
	{

		

		String[] partesHora1 = phora1.split(":");
		String []partesHora2 = phora2.split(":");


		int hora1 = Integer.parseInt(partesHora1[0]);
		int hora2 = Integer.parseInt(partesHora2[0]);

		int minutos1 = Integer.parseInt(partesHora1[1]);
		int minutos2 = Integer.parseInt(partesHora2[1]);

		int segundos1 = Integer.parseInt(partesHora1[2]);
		int segundos2 = Integer.parseInt(partesHora2[2]);




		int diferenciaHora = hora1 - hora2;
		int diferenciaMinutos = minutos1 - minutos2;
		int diferenciaSegundos = segundos1 - segundos2;

		int []diferencias  = {diferenciaHora, diferenciaMinutos, diferenciaSegundos};
		return diferencias;

	}

	
	//La diferencia absoluta entre dos horas en segundos
	public int difereciaDosHoras (String phora1, String phora2)
	{

		int []diferencias = formatearHoras(phora1, phora2);

		int diferenciaHora = diferencias[0];
		int diferenciaMinutos = diferencias[1];
		int diferenciaSegundos = diferencias[2];


		if(diferenciaSegundos < 0)
		{
			diferenciaSegundos += 60;
			diferenciaMinutos--;

		}

		if(diferenciaMinutos < 0)
		{
			diferenciaMinutos += 60;
			diferenciaHora--;
		}

		if(diferenciaHora <0)
		{

			diferenciaHora += 24;

		}


		int segundos = (diferenciaHora*60*60) + (diferenciaMinutos*60) + diferenciaSegundos;

		return segundos;
	}




	//1 si pHora es m�s tarde que pHora2, -1 si es m�s temprano
	public int compararHoras(String phora1, String phora2 )
	{
		phora1 = cambiarA24horas(phora1);
		phora2 = cambiarA24horas(phora2);
		
//		System.out.println("Hora1 " + phora1);
//		System.out.println("Hora2 " + phora2);
		int [] diferencias = formatearHoras(phora1, phora2);

		int diferenciaHora = diferencias[0];
		int diferenciaMinutos = diferencias[1];
		int diferenciaSegundos = diferencias[2];

		
		
		if(diferenciaHora > 0)
		{
			return 1;
		}
		else if (diferenciaHora < 0)
		{
			return -1;
		}

		else if(diferenciaMinutos > 0)
		{
			return 1;
		}else if ( diferenciaMinutos < 0){
			return -1;
		}


		else if(diferenciaSegundos > 0)
		{
			return 1; 
		}else if(diferenciaSegundos < 0){
			return -1;
		}
		
		
		
			return 0;
		
		
	}
	
	
	
	

}
