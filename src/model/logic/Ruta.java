package model.logic;

import java.util.Iterator;

import model.data_structures.DoubleLinkedList;
import model.data_structures.Nodo;
import model.vo.StopTimesVO;
import model.vo.StopVO;

public class Ruta implements Comparable<Ruta>{


	


	private int routeId;
	private String agencyId;
	private String routeShortName;
	private String routeLongName;
	private String routeDesc; 
	private int routeType;
	private String routeUrl;
	private String routeColor; 	
	private String routeTextColor;
	
	private DoubleLinkedList<Trip> listaTrips;
	private DoubleLinkedList<StopVO> listaStops;
	private DoubleLinkedList<StopTimesVO> listaStoptimes;
	
	private boolean rutaTrabaja;







	public Ruta(int pRouteId, String pAgencyId, String pRouteShortName,String pRouteLongName, String pRouteDesc, int pRouteType, String pRouteUrl, String pRouteColor, String pRouteTextColor)
	{
		listaTrips = new DoubleLinkedList<>(); 
		listaStops = new DoubleLinkedList<>();
		listaStoptimes = new DoubleLinkedList<>();
		
		routeId = pRouteId;
		setAgencyId(pAgencyId); 
		setRouteShortName(pRouteShortName);
		routeLongName = pRouteLongName;
		routeDesc = pRouteDesc;
		routeType = pRouteType;
		routeUrl = pRouteUrl;
		routeColor = pRouteColor;
		routeTextColor = pRouteColor;


	}

	

	
	public Trip buscarTrip(int pTripId)
	{
		
		
		Nodo<Trip> nodoActual = listaTrips.cabeza();
		
		while(nodoActual != null)
		{
			if(nodoActual.darElemento().getTripId() == pTripId)
			{
				return nodoActual.darElemento();
			}
			nodoActual = nodoActual.darSiguiente();
						
		}
		
	
		return null;
	}

	


	
	public void addTrip(Trip n)
	{
		
		listaTrips.add(n);
	} 	


	
	public StopVO primeraParada()
	{
		
		return listaTrips.primerElemento().darStops().primerElemento();
		
	}
		
		
		
	public void crearListaDeStopTimes()
	{
		for (Trip trip : listaTrips) {
			
			listaStoptimes.unirDosListas(trip.darStopTimes());
			
		}
		
		
	}
	
	public DoubleLinkedList<StopTimesVO> getListaStopTimes()
	{
		return listaStoptimes; 
	}
	

	public boolean tieneTrip(DoubleLinkedList<String> listaAbuscar)
	{

		for (String voTripBuscar : listaAbuscar) {


			if(buscarTrip(Integer.parseInt(voTripBuscar)) != null)
			{
				return true;
			}
			
			
		}
		return false;
	}

	
	
	
	
	public DoubleLinkedList<Trip> getListaTrips()
	{
		return listaTrips; 
	}


	public DoubleLinkedList<StopVO> getListaStops()
	{
		return listaStops; 
	}
	
	
	/**
	 * @return id - Route's id number
	 */
	public int darId() {

		return routeId;
	}

	/**
	 * @return name - route name
	 */
	public String getName() {
	
		return routeLongName;
	}

	public String getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(String agencyId) {
		this.agencyId = agencyId;
	}

	public String getRouteShortName() {
		return routeShortName;
	}

	public void setRouteShortName(String routeShortName) {
		this.routeShortName = routeShortName;
	}




	public boolean getRutaTrabaja() {
		return rutaTrabaja;
	}




	public void setRutaTrabaja(boolean rutaTrabaja) {
		this.rutaTrabaja = rutaTrabaja;
	}




	@Override
	public int compareTo(Ruta o) {

		
		if(routeId < o.darId())
		{
			return -1;
		}
		else if(routeId > o.darId())
		{
			return 1;
		}
		
		return 0;
	}


	
	
	
	
	
	
	

	
	
	
	
	
	
}
