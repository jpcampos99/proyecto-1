package model.data_structures;

import java.util.Iterator;

import model.logic.Ruta;

public class DoubleLinkedList <T extends Comparable<T>> implements Iterable<T>, Comparable<DoubleLinkedList<T>>{


	private DoubleLinkedList<Nodo<T>> bookmarks; 

	//Las	 listas	
	//doblemente	encadenadas	deben	tener	una	referencia	a	su	primer	y	�ltimo	
	//elemento,	y	cada nodo	en	la	lista	debe	poder	acceder	a	su	sucesor	y	
	//predecesor	(excepto	en	el	caso	del	primer	y	�ltimo	elemento	de	la	lista).


	private Nodo<T> cabeza; 
	private Nodo<T> cola; 
	private int size = 0; 

	public DoubleLinkedList( )
	{
		cabeza = new Nodo<T>(null, null, null); 
		cola = new Nodo<T>(null, cabeza, null); 
		cabeza.cambiarSiguiente(cola);		

	}


	public void setCabeza(Nodo<T> head)
	{
		cabeza = head; 
		size++; // El tama�o de esta lista no est� bien
		cola = null;
	}
	public boolean estaVacio( )
	{
		return size == 0; 
	}

	public T primero( )
	{
		if( estaVacio() )
		{
			return null; 
		}
		else
		{
			return cabeza.darElemento(); 
		}	
	}

	public T ultimo( )
	{
		if( estaVacio() )
		{
			return null; 
		}
		else
		{
			return cola.darElemento(); 
		}
	}



	public Integer getSize() {

		return size;
	}



	public Iterator<T> iterator() {

		Iterator<T> iter = new Iterator<T>() { 


			final Nodo<T> firstNode = cabeza;
			Nodo<T> currentNode = null;

			public boolean hasNext() {
				if (estaVacio()) {
					return false;
				} else if (currentNode == null){
					return true;
				} else if (currentNode.siguiente == null){
					return false;
				}
				return true;
			}

			public T next() {
				if (estaVacio()){
					System.out.println("No hay mas elementos");
				} else if (currentNode == null){
					this.currentNode = firstNode;
					return currentNode.darElemento();
				} else if (currentNode.siguiente == null) {
					System.out.println("No hay mas elementos");

				}
					this.currentNode = currentNode.siguiente;
					return currentNode.darElemento();
				
				
			}
		};
		return iter;



	}

	
	public void unirDosListas(DoubleLinkedList<T> pLista)
	{
		
		if (estaVacio())
		{
			cabeza = pLista.cabeza;
			cola = pLista.cola;
			size = pLista.getSize();
		}
		else{
			
			Nodo <T> pCabeza = pLista.cabeza;
			Nodo <T> pCola= pLista.cola;
			
			cola.cambiarSiguiente(pCabeza);
			pCabeza.cambiarAnterior(cola);
			
			size += pLista.getSize();
			
			
		}
		
		
		
	}
	

	public T primerElemento() 
	{
		return cabeza.elemento;

	}




	//Agrega al principio
	public void add(T elem) {


		Nodo<T> nodo1 = new Nodo<T>(elem, null, null); 

		if(estaVacio())
		{
			cabeza = nodo1;
			cola = nodo1;
			size++;
		}
		else 
		{
			Nodo<T> viejoPrimero = cabeza;

			cabeza = nodo1;

			cabeza.cambiarSiguiente(viejoPrimero);

			viejoPrimero.cambiarAnterior(cabeza);
			size++;
		}

	}


	
	public void nuevaCabezaPos(int k)
	{
		
		Nodo<T> nuevaCabeza = getNodo(k);

		Nodo<T>  anterior = nuevaCabeza.darAnterior();
		
		if(anterior != null)
		{
			anterior.cambiarSiguiente(null);
		}
		
		nuevaCabeza.cambiarAnterior(null);
		size -= k;
		cabeza = nuevaCabeza; 
		
		
		
	}

	public void intercambiar(Nodo<T> nodo1, Nodo<T> nodo2)
	{

		if (nodo1 == nodo2)
			return;

		if (nodo1.siguiente == nodo2) { // al lado

			nodo1.siguiente = nodo2.siguiente;
			nodo2.anterior = nodo1.anterior;


			if (nodo1.siguiente != null)
				nodo1.siguiente.anterior = nodo1;

			if (nodo2.anterior != null)
				nodo2.anterior.siguiente = nodo2;


			nodo2.siguiente = nodo1;
			nodo1.anterior = nodo2;


		} else {
			Nodo<T> p = nodo2.anterior;
			Nodo<T> n = nodo2.siguiente;

			nodo2.anterior = nodo1.anterior;
			nodo2.siguiente = nodo1.siguiente;

			nodo1.anterior = p;
			nodo1.siguiente = n;

			if (nodo2.siguiente != null)
				nodo2.siguiente.anterior = nodo2;
			if (nodo2.anterior != null)
				nodo2.anterior.siguiente = nodo2;

			if (nodo1.siguiente != null)
				nodo1.siguiente.anterior = nodo1;
			if (nodo1.anterior != null)
				nodo1.anterior.siguiente = nodo1;

		}

		if (nodo1 == cabeza)
		{	
			cabeza = nodo2;
		}
		else if (nodo2 == cabeza)
		{	
			cabeza = nodo1;
		}

		if(nodo1 == cola)
		{	
			cola = nodo2;
		}
		else if(nodo2 == cola)
		{	
			cola = nodo1;
		}

	}

	public void addAtEnd(T aInsertar) {


		Nodo<T> n = new Nodo<>(aInsertar, null, null);


		if( estaVacio() )
		{
			cabeza = n; 
			cola = n;
			size++;
		}
		else
		{

			Nodo<T> viejaCola = cola;


			cola = n;
			n.cambiarAnterior(viejaCola);
			viejaCola.cambiarSiguiente(n);
			size++;
		}
	}



	public void addAtk(T elem, int k) {


		Nodo<T> nodo1 = new Nodo<T>(elem, null, null);

		if(k > size + 1)
		{
			System.out.println("No se puede agregar we");
			return;
		}

		if(estaVacio())
		{

			cabeza = nodo1; 
			cola = nodo1;
			size++;
		}

		else if(k == 0)//Si agrega en la primera posici�n
		{
			add(elem);
		}

		else if(k == size)//si agrega en la �ltima posici�n
		{
			addAtEnd(elem);
		}

		else
		{
			Nodo<T> nodoDespues = getNodo(k);
			Nodo<T> nodoAntes = nodoDespues.darAnterior();


			nodo1.cambiarSiguiente(nodoDespues);
			nodo1.cambiarAnterior(nodoAntes);

			nodoDespues.cambiarAnterior(nodo1);
			nodoAntes.cambiarSiguiente(nodo1);

			size++;
		}


	}


	public T getElement(int k) {


		if(estaVacio()|| k > size -1 )
		{
			return null;
		}

		else
		{

			int i = 0;
			Nodo<T> nodo = cabeza;

			while( nodo.siguiente != null)
			{

				if(i == k)
				{
					return nodo.darElemento();
				}
				else{
					nodo = nodo.darSiguiente();
					i++;
				}


			}

			return nodo.darElemento();
		}
	}



	public Nodo<T> getNodo(int k) {


		if(estaVacio()	)
		{
			return null;
		}
		else
		{

			int i = 0;
			Nodo<T> nodo = cabeza;

			while( nodo.siguiente != null)
			{

				if(i == k)
				{
					return nodo;
				}
				else{
					nodo = nodo.darSiguiente();
					i++;
				}


			}

			return nodo;
		}
	}


	public Nodo<T> getCurrentElement() {
		// TODO Auto-generated method stub
		return null;
	}



	//elimina el primero

	public void delete() {


		if(estaVacio())
		{
			System.out.println("No hay elementos para eliminar");
			return;
		}

		Nodo <T> nuevoPrimero = cabeza.darSiguiente();


		cabeza = nuevoPrimero;
		nuevoPrimero.cambiarAnterior(null);
		size--;


	}



	public void eliminarNodo(Nodo<T> nodoAEliminar)
	{
		if(nodoAEliminar == cola)
		{
			Nodo<T> nodoAnterior = nodoAEliminar.darAnterior();
			nodoAnterior.cambiarSiguiente(null);
			cola = nodoAnterior;
			size--;
		}
		else if (nodoAEliminar == cabeza  )
		{
			Nodo<T> nodoSiguiente = nodoAEliminar.darSiguiente();
			nodoAEliminar.cambiarSiguiente(null);
			cabeza = nodoSiguiente; 
			size--;
		}
		else if(nodoAEliminar.darAnterior() == null)
		{
		
			nodoAEliminar.cambiarSiguiente(null);
			size--;
		}

		else
		{

			Nodo<T> nodoAnterior = nodoAEliminar.darAnterior();
			Nodo<T> nodoSiguiente = nodoAEliminar.darSiguiente();


			nodoAnterior.cambiarSiguiente(nodoSiguiente);
			nodoSiguiente.cambiarAnterior(nodoAnterior);

			nodoAEliminar.cambiarAnterior(null);
			nodoAEliminar.cambiarSiguiente(null);
			size--;
		}



	}

	public void deleteAtk(int k) {

		if(estaVacio())
		{
			System.out.println("No hay elementos para eliminar");
			return;
		}


		Nodo<T> nodoAEliminar = getNodo(k);

		eliminarNodo(nodoAEliminar);


	}


	public Nodo<T> cabeza() {
		// TODO Auto-generated method stub
		return cabeza;
	}


	public T elementoCola()
	{
		return cola.darElemento();
	}


	public void addBookmark()
	{
		if(bookmarks == null)
		{
			bookmarks = new DoubleLinkedList<>(); 
		}

		bookmarks.addAtEnd(cola);
	}

	public DoubleLinkedList<Nodo<T>> darBookmarks()
	{
		return bookmarks; 
	}

	public Nodo<T> cola() {
		// TODO Auto-generated method stub
		return cola;
	}

	@Override
	public int compareTo(DoubleLinkedList<T> o) {
		// TODO Auto-generated method stub
		return 0;
	}









}
