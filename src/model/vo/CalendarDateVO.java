package model.vo;

public class CalendarDateVO implements Comparable<CalendarDateVO>
{
	private int serviceId;
	private String date;
	private int exception;
	
	
	public CalendarDateVO(int serviceId, String date, int exception) {
		super();
		this.setServiceId(serviceId);
		this.setDate(date);
		this.setException(exception);
	}


	public int getServiceId() {
		return serviceId;
	}


	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}


	public String getDate() {
		return date;
	}


	public void setDate(String date) {
		this.date = date;
	}


	public int getException() {
		return exception;
	}


	public void setException(int exception) {
		this.exception = exception;
	}


	@Override
	public int compareTo(CalendarDateVO o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	
	
	
	
}
