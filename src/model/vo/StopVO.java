package model.vo;


public class StopVO implements Comparable<StopVO>{

	
	private int stopId;
	private int stopCode;
	private String stopName; 	
	private String stopDesc;
	private double stopLat;
	private double stopLon;
	private String zoneId;
	private String stopUrl;
	private String locationType;
	private int parentStation;

	
	private int numIncidentes;
	
	public StopVO(int pStopId, int pStopCode, String pStopName, String pStopDesc, double pStopLat, double pStopLon, String pZoneId, String pStopUrl, String pLocationType, int pPartenStation)
	
	{
		numIncidentes = 0;
		
		stopId = pStopId;
		setStopCode(pStopCode);
		stopName = pStopName;
		stopDesc = pStopDesc;
		setStopLat(pStopLat);
		setStopLon(pStopLon); 
		setZoneId(pZoneId); 
		locationType = pLocationType; 
		parentStation = pPartenStation;
		
	}
	
	public void aumentarIncidentes()
	{
		numIncidentes++;
	}
	
	/**
	 * @return id - stop's id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return stopId;
	}

	/**
	 * @return name - stop name
	 */
	public String getName() {
		// TODO Auto-generated method stub
		return stopName;
	}


	public String getZoneId() {
		return zoneId;
	}


	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}


	@Override// compara por stopCode
	public int compareTo(StopVO o) {
		if(stopCode > o.getStopCode())
		{
			return 1;
		}else if (stopCode < o.getStopCode())
		{
			return -1; 
		}
		
		return 0;
	}

	
	public int compareNumeroIncidentes(StopVO o)
	{
		if(numIncidentes > o.getNumeroIncidentes())
		{
			return 1;
		}else if (numIncidentes< o.getNumeroIncidentes())
		{
			return -1; 
		}
		return 0; 
	}

	public int getNumeroIncidentes() {
		// TODO Auto-generated method stub
		return numIncidentes;
	}


	public double getStopLat() {
		return stopLat;
	}


	public void setStopLat(double stopLat) {
		this.stopLat = stopLat;
	}


	public double getStopLon() {
		return stopLon;
	}


	public void setStopLon(double stopLon) {
		this.stopLon = stopLon;
	}


	public int getStopCode() {
		return stopCode;
	}


	public void setStopCode(int stopCode) {
		this.stopCode = stopCode;
	}

	
	
	
	
}
