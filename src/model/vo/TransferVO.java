package model.vo;

import model.data_structures.DoubleLinkedList;

public class TransferVO implements Comparable<TransferVO>{

	private int fromStopId;
	private int toStropId;
	
	private StopVO fromStop;
	private StopVO toStop; 
	
	private int transferType;
	private String minTransferTime;
	
	
	private DoubleLinkedList<StopTimesVO> listaStopTimes;
	
	private String primeraHora;
	private String ultimaHora;
	
	
	
	public TransferVO(int fromStopId, int toStropId, int transferType, String minTransferTime) {
		super();
		listaStopTimes = new DoubleLinkedList<>();
		
		this.setFromStopId(fromStopId);
		this.setToStropId(toStropId);
		this.setTransferType(transferType);
		this.setMinTransferTime(minTransferTime);
	}


	
	public void addStopTimes(StopTimesVO stopTime)
	{
		listaStopTimes.addAtEnd(stopTime);
	}
	
	
	public int getFromStopId() {
		return fromStopId;
	}



	public void setFromStopId(int fromStopId) {
		this.fromStopId = fromStopId;
	}



	public int getToStopId() {
		return toStropId;
	}



	public void setToStropId(int toStropId) {
		this.toStropId = toStropId;
	}



	public int getTransferType() {
		return transferType;
	}



	public void setTransferType(int transferType) {
		this.transferType = transferType;
	}



	@Override
	public int compareTo(TransferVO o) {
		
		int thisTransfer = Integer.parseInt(minTransferTime); 
		int thatTransfer = Integer.parseInt(o.minTransferTime);
		
		if(thisTransfer > thatTransfer)
		{
			return 1; 
		}else if( thisTransfer < thatTransfer)
		{
			return -1;
		}
			
			
			return 0; 
	}



	public DoubleLinkedList<StopTimesVO> getListadeParadas() {
		// TODO Auto-generated method stub
		return listaStopTimes;
	}



	public String getMinTransferTime() {
		return minTransferTime;
	}



	public void setMinTransferTime(String minTransferTime) {
		this.minTransferTime = minTransferTime;
	}




	public StopVO getFromStop() {
		return fromStop;
	}




	public void setFromStop(StopVO fromStop) {
		this.fromStop = fromStop;
	}




	public StopVO getToStop() {
		return toStop;
	}




	public void setToStop(StopVO toStop) {
		this.toStop = toStop;
	}




	public String getPrimeraHora() {
		return primeraHora;
	}




	public void setPrimeraHora(String primeraHora) {
		this.primeraHora = primeraHora;
	}



	
	
	
	
	
	

}
