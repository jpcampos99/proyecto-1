package model.vo;

public class StopTimesVO implements Comparable<StopTimesVO> {
	


	private StopVO stop;
	private int tripId; 
	private String arrivalTime; 
	private String departureTime;
	private int stopId;
	private int stopSequence;
	private String stopHeadsign;
	private int pickup_type;
	private int dropOffType;
	private double shape_dist_traveled;
	
	
	public StopTimesVO(int tripId, String arrivalTime, String departureTime, int stopId, int stopSequence,
			String stopHeadsign, int pickup_type, int dropOffType, double pshape_dist_traveled) {
		super();
		this.setTripId(tripId);
		this.setArrivalTime(arrivalTime);
		this.setDepartureTime(departureTime);
		this.setStopId(stopId);
		this.stopSequence = stopSequence;
		this.stopHeadsign = stopHeadsign;
		this.pickup_type = pickup_type;
		this.dropOffType = dropOffType;
		shape_dist_traveled = pshape_dist_traveled;
	}



	public int getTripId() {
		return tripId;
	}



	public void setTripId(int tripId) {
		this.tripId = tripId;
	}



	public String getArrivalTime() {
		return arrivalTime;
	}



	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}



	public String getDepartureTime() {
		return departureTime;
	}



	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}



	public int getStopId() {
		return stopId;
	}



	public void setStopId(int stopId) {
		this.stopId = stopId;
	}



	public StopVO getStop() {
		return stop;
	}



	public void setStop(StopVO stop) {
		this.stop = stop;
	}



	@Override
	public int compareTo(StopTimesVO o) {
		// TODO Auto-generated method stub
		return 0;
	}


	public Double getDistTraveled() {
		return shape_dist_traveled;
	}
	
	
	
	

	
	

}
