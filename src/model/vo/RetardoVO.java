package model.vo;

import model.logic.Trip;

public class RetardoVO implements Comparable<RetardoVO>{

	private Trip trip;
	private StopTimesVO stopTime;
	private int tiempoDeRetraso;
	
	public RetardoVO(Trip trip, int tiempoDeRetraso, StopTimesVO stoptime) {
	
		this.setTrip(trip);
		this.setTiempoDeRetraso(tiempoDeRetraso);
		this.stopTime = stoptime; 
	}

	public Trip getTrip() {
		return trip;
	}

	public void setTrip(Trip trip) {
		this.trip = trip;
	}

	public int getTiempoDeRetraso() {
		return tiempoDeRetraso;
	}

	public void setTiempoDeRetraso(int tiempoDeRetraso) {
		this.tiempoDeRetraso = tiempoDeRetraso;
	}

	
	public StopTimesVO getStopTime() {
		return stopTime;
	}

	@Override
	public int compareTo(RetardoVO o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	
	
	
}
