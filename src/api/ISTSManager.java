package api;

import java.io.File;

import model.data_structures.DoubleLinkedList;
import model.data_structures.LinkedStack;
import model.logic.Ruta;
import model.logic.Trip;
import model.vo.RetardoVO;
import model.vo.ServiceVO;
import model.vo.StopVO;
import model.vo.TransferVO;

public interface ISTSManager {

	
//	Carga	 toda	 la	 informaci�n	 est�tica	 necesaria	 para	 la	 operaci�n	 del	 sistema.		
//	(Archivo	de	rutas,	viajes,	paradas,	etc.)
	
	public void ITScargarGTFS();
		
//	Carga	la	informaci�n	en	tiempo	real	de	los	buses	en	para	fecha	determinada.	
	public void ITScargarTR(String fecha);
	
//	Retorna	una	lista	de	rutas	que	son	prestadas	por	una	empresa	determinada,	en	
//	una	fecha	determinada.
	public  DoubleLinkedList<Ruta> ITSrutasPorEmpresa(String nombreEmpresa, String fecha);
	
//	Retorna	una	lista	de	viajes	retrasados	para	una	ruta	en	una	fecha	determinada
	public DoubleLinkedList<Trip> ITSviajesRetrasadosRuta(String idRuta, String fecha); 
	
	
//	Retorna	una	lista	de	Paradas	en	las	que	hubo	un	retraso	en	una	fecha	dada
	public DoubleLinkedList<StopVO> ITSparadasRetrasadasFecha(String fecha);
	
//	Retorna	una	lista	de	paradas,	las	cuales	conforman	todos	los	posibles	transbordos	
//	que	se	derivan	a	partir	de	la	primera	parada	de	dicha	ruta
	public DoubleLinkedList <TransferVO> ITStransbordosRuta(String idRuta);
	
//	Retorna	una	lista	en	la	que	en	cada	posici�n	se	tiene	una	lista	con	la	informaci�n	de	
//	una	parada	espec�fica.		A	su	vez	en	cada	posici�n	de	esta	lista,	se	tiene	una	lista	con	
//	los	viajes		de	la	parada	que	se	est�	analizando	(Punto	5A)
	public  DoubleLinkedList<DoubleLinkedList<Trip>> ITSrutasPlanUtilizacion (String fecha, String
	horaInicio, String horaFin, DoubleLinkedList<String> secuenciaDeParadas);
	
//	Retorna	una	lista	de	rutas	que	son	prestadas	por	una	empresa	determinada,	en	una	fecha	determinada.	
//	La	lista	est�	ordenada	por	cantidad	total	de	paradas
	public DoubleLinkedList<Ruta> ITSrutasPorEmpresaParadas(String nombreEmpresa, String fecha);
	
	
//	Retornar una lista con todos los viajes en los que despu�s de un retardo, todas las
//	paradas siguientes tuvieron retardo, para una ruta espec�fica en una fecha espec�fica.
//	Se debe retornar una lista ordenada por el id del viaje, y la localizaci�n de las
//	paradas, de mayor a menor en tiempo de retardo.
	public DoubleLinkedList<Trip> ITSviajesRetrasoTotalRuta(String idRuta, String fecha);
//	
//	Retorna	una	lista	ordenada	con	rangos	de	hora	en	los	que	mas	retardos	
//	hubo.
	public DoubleLinkedList<String> ITSretardoHoraRuta (String idRuta, String Fecha ); 	
	
//	Retorna	una	lista	con	los	viajes	para	ir	de	la	parada	de	inicio	a	la	parada	
//	final	en	una	fecha	y	franja	horaria	determinada.
	public DoubleLinkedList <Trip> ITSbuscarViajesParadas(String idOrigen, String idDestino,String fecha, String horaInicio, String horaFin) ;
	
//	Retorna la ruta que tiene menos retardo promedio en distancia de sus viajes
//	en una fecha dada. El retardo promedio en distancia de un viaje corresponde
//	al total de los tiempos de retardo de sus paradas dividido por la distancia
//	recorrida.
	public Ruta ITSrutaMenorRetardo (String idRuta, String fecha);
	
//	Inicializa	las	estructuras	de	datos	necesarias
	public Void ITSInit();
	
//	Retorna	una	lista	con	los	servicios	que	mas	distancia	recorren	en	una	
//	fecha.		La	lista	est�	ordenada	por	distancia.
	public DoubleLinkedList<ServiceVO>	ITSserviciosMayorDistancia (String fecha);
	
//	Retorna	una	lista	con	todos	los	retardos	de	un	viaje	en	una	fecha	dada.
	public DoubleLinkedList<RetardoVO>	ITSretardosViaje (String fecha, String idViaje);
	
//	Retorna	una	lista	con	todas	las	paradas	del	sistema	que	son	compartidas	
//	por	mas	de	una	ruta	en	una	fecha	determinada
	public DoubleLinkedList <StopVO>	ITSparadasCompartidas (String fecha);

	
}
